function syncDropDowns() {
    var selected = [];
    var deselected = [];
    if (systemAssetIsActive) {
        selected.push("System Asset");
    } else {
        deselected.push("System Asset")
    }
    if (businessAssetIsActive) {
        selected.push("Business Asset")
    } else {
        deselected.push("Business Asset");
    }

    if (securityCriterionIsActive) {
        selected.push("Security Criterion")
    } else {
        deselected.push("Security Criterion");
    }
    if (riskIsActive) {
        selected.push("Risk")
    } else {
        deselected.push("Risk");
    }
    if (eventIsActive) {
        selected.push("Event")
    } else {
        deselected.push("Event");
    }
    if (impactIsActive) {
        selected.push("Impact")
    } else {
        deselected.push("Impact");
    }
    if (attackMethodIsActive) {
        selected.push("Attack Method")
    } else {
        deselected.push("Attack Method");
    }
    if (vulnerabilityIsActive) {
        selected.push("Vulnerability")
    } else {
        deselected.push("Vulnerability");
    }
    if (threatAgentIsActive) {
        selected.push("Threat Agent");
    } else {
        deselected.push("Threat Agent");
    }
    if (threatIsActive) {
        selected.push("Threat")
    } else {
        deselected.push("Threat");
    }
    if (securityRequirementIsActive) {
        selected.push("Security Requirement")
    } else {
        deselected.push("Security Requirement");
    }

    if (riskTreatmentIsActive) {
        selected.push("Risk Treatment")
    } else {
        deselected.push("Risk Treatment");
    }
    if (controlIsActive) {
        selected.push("Control")
    } else {
        deselected.push("Control");
    }

    $(".selectBox").each(function (i, object) {
        $(object).multiselect('select', selected);
    });

    $(".selectBox").each(function (i, object) {
        $(object).multiselect('deselect', deselected);
    });
}

function drawText() {
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");
    ctx.font = "italic 30px Arial";
    ctx.fillStyle = "black";
    ctx.fillText("Use Case diagrams", 20, 30);

    ctx.font = "italic 10px Arial";
    ctx.fillText("Actor OR Misuser", 60, 120);
    ctx.fillText("Use OR Misuse Case", 450, 120);
    ctx.font = "10px Arial";
    ctx.fillText("Actor", 30, 200);
    ctx.fillText("Misuser", 150, 200);
    ctx.fillText("Include", 380, 30);
    ctx.fillText("Extend", 580, 30);
    ctx.fillText("Communication", 255, 120);
    ctx.fillText("Constraint Of", 160, 270);
    ctx.fillText("Use Case", 330, 220);
    ctx.fillText("Vulnerability", 460, 220);
    ctx.fillText("Exploit", 620, 220);
    ctx.fillText("Security Criterion", 150, 370);
    ctx.fillText("Negate", 170, 470);
    ctx.fillText("Security Use Case", 350, 370);
    ctx.fillText("Threaten", 450, 320);
    ctx.fillText("Harm", 350, 420);
    ctx.fillText("Mitigate", 520, 370);
    ctx.fillText("Impact", 520, 470);
    ctx.fillText("Misuse Case", 710, 320);
    ctx.fillText("Lead To", 710, 470);

    //relationship labels

    ctx.fillText("Initiates", 180, 115);
    ctx.fillText("InteractsWith", 360, 115);
    ctx.fillText("IncludedCase", 330, 70);
    ctx.fillText("IncludingCase", 410, 70);
    ctx.fillText("ExtendedCase", 480, 20);
    ctx.fillText("ExtendingCase", 610, 80);
    ctx.fillText("Restrictor", 140, 320);
    ctx.fillText("Restrictee", 235, 220);
    ctx.fillText("Negatee", 140, 420);
    ctx.fillText("Negator", 340, 480);
    ctx.fillText("From", 440, 430);
    ctx.fillText("From", 640, 460);
    ctx.fillText("To", 300, 320);
    ctx.fillText("From", 450, 360);
    ctx.fillText("To", 620, 360);
    ctx.fillText("To", 750, 400);
    ctx.fillText("TargetedBy", 400, 280);
    ctx.fillText("Targets", 580, 310);
    ctx.fillText("Includes", 410, 210);
    ctx.fillText("Targets", 540, 210);
    ctx.fillText("ExploitedBy", 650, 270);


};

function drawArrow(canvasId, tipx, tipy, direction, color) {
    var canvas = document.getElementById(canvasId);
    var ctx = canvas.getContext("2d");
    ctx.beginPath();
    ctx.moveTo(tipx, tipy);
    if (direction === "UP") {
        ctx.lineTo(tipx + 5, tipy + 5);
        ctx.lineTo(tipx - 5, tipy + 5);
    } else if (direction == "RIGHT") {
        ctx.lineTo(tipx - 5, tipy + 5);
        ctx.lineTo(tipx - 5, tipy - 5);
    } else if (direction == "DOWN") {
        ctx.lineTo(tipx - 5, tipy - 5);
        ctx.lineTo(tipx + 5, tipy - 5);
    } else if (direction == "LEFT") {
        ctx.lineTo(tipx + 5, tipy - 5);
        ctx.lineTo(tipx + 5, tipy + 5);
    }
    ctx.closePath();
    ctx.fillStyle = color;
    ctx.fill();
    if (color === "white") {
        ctx.stroke();
    }
};

function drawLines() {
    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");
    ctx.moveTo(55, 180);
    ctx.lineTo(55, 160);
    ctx.lineTo(170, 160);
    ctx.lineTo(170, 180);
    ctx.moveTo(100, 160);
    ctx.lineTo(100, 140);
    ctx.moveTo(140, 120);
    ctx.lineTo(250, 120);

    ctx.moveTo(330, 120);
    ctx.lineTo(450, 120);

    ctx.moveTo(450, 100);
    ctx.lineTo(400, 100);
    ctx.lineTo(400, 45);

    ctx.moveTo(430, 30);
    ctx.lineTo(470, 30);
    ctx.lineTo(470, 100);

    ctx.moveTo(500, 100);
    ctx.lineTo(500, 30);
    ctx.lineTo(550, 30);

    ctx.moveTo(600, 45);
    ctx.lineTo(600, 120);
    ctx.lineTo(530, 120);

    ctx.moveTo(190, 250);
    ctx.lineTo(190, 230);
    ctx.lineTo(320, 230);

    ctx.moveTo(190, 290);
    ctx.lineTo(190, 350);

    ctx.moveTo(190, 390);
    ctx.lineTo(190, 450);

    ctx.moveTo(230, 490);
    ctx.lineTo(500, 490);

    ctx.moveTo(580, 470);
    ctx.lineTo(700, 470);


    ctx.moveTo(140, 120);
    ctx.lineTo(250, 120);

    ctx.moveTo(490, 140);
    ctx.lineTo(490, 200);

    ctx.moveTo(360, 200);
    ctx.lineTo(360, 180);
    ctx.lineTo(740, 180);
    ctx.lineTo(740, 300);

    //Use case -> vulnerability
    ctx.moveTo(400, 220);
    ctx.lineTo(450, 220);

    //vulnerabilit->exploit
    ctx.moveTo(530, 220);
    ctx.lineTo(600, 220);

    //HARM -> Use case
    ctx.moveTo(330, 400);
    ctx.lineTo(330, 240);

    //security use case -> use case
    ctx.moveTo(360, 350);
    ctx.lineTo(360, 240);

    //use case -> threaten
    ctx.moveTo(390, 240);
    ctx.lineTo(390, 320);
    ctx.lineTo(430, 320);

    //threaten -> misusecase
    ctx.moveTo(510, 320);
    ctx.lineTo(700, 320);

    //security use case -> mitigate
    ctx.moveTo(430, 370);
    ctx.lineTo(500, 370);

    //mitigate -> misuse case
    ctx.moveTo(580, 370);
    ctx.lineTo(720, 370);
    ctx.lineTo(720, 340);

    //harm -> impact
    ctx.moveTo(400, 420);
    ctx.lineTo(520, 420);
    ctx.lineTo(520, 450);

    //impact -> lead to
    ctx.moveTo(580, 470);
    ctx.lineTo(700, 470);

    //misuse case -> lead to
    ctx.moveTo(740, 340);
    ctx.lineTo(740, 450);

    //exploit -> misusecase
    ctx.moveTo(680, 220);
    ctx.lineTo(720, 220);
    ctx.lineTo(720, 300);


    ctx.stroke();
};

function drawArrows() {
    drawArrow("canvas", 100, 140, "UP", "white");
    drawArrow("canvas", 230, 110, "RIGHT", "black");
    drawArrow("canvas", 430, 110, "RIGHT", "black");
    drawArrow("canvas", 370, 80, "UP", "black");
    drawArrow("canvas", 450, 80, "DOWN", "black");
    drawArrow("canvas", 500, 5, "RIGHT", "black");
    drawArrow("canvas", 620, 100, "DOWN", "black");


    drawArrow("canvas", 300, 215, "RIGHT", "black");
    drawArrow("canvas", 180, 300, "UP", "black");
    drawArrow("canvas", 180, 430, "DOWN", "black");


    drawArrow("canvas", 440, 215, "RIGHT", "black");
    drawArrow("canvas", 550, 215, "LEFT", "black");
    drawArrow("canvas", 690, 250, "UP", "black");

    drawArrow("canvas", 400, 300, "DOWN", "black");
    drawArrow("canvas", 560, 310, "LEFT", "black");

    drawArrow("canvas", 320, 315, "UP", "black");
    drawArrow("canvas", 435, 360, "LEFT", "black");
    drawArrow("canvas", 640, 360, "RIGHT", "black");

    drawArrow("canvas", 320, 480, "LEFT", "black");
    drawArrow("canvas", 480, 430, "RIGHT", "black");
    drawArrow("canvas", 680, 460, "RIGHT", "black");
    drawArrow("canvas", 755, 410, "DOWN", "black");

    drawArrow("canvas", 490, 140, "UP", "white");
};

function selectAssetRelated() {
    assetIsActive = true;
    systemAssetIsActive = true;
    businessAssetIsActive = true;
    securityCriterionIsActive = true;
    riskIsActive = false;
    impactIsActive = false;
    eventIsActive = false;
    attackMethodIsActive = false;
    vulnerabilityIsActive = false;
    threatAgentIsActive = false;
    threatIsActive = false;
    riskTreatmentIsActive = false;
    securityRequirementIsActive = false;
    controlIsActive = false;
    var active = ["System Asset", "Business Asset", "Security Criterion"];
    for (var i = 0; i < active.length; i++) {
        syncDropDowns();
    }
    var inActive = ["Risk", "Impact", "Event", "Attack Method", "Vulnerability", "Threat", "Threat Agent", "Risk Treatment", "Security Requirement", "Control"];
    for (var i = 0; i < inActive.length; i++) {
        syncDropDowns();
    }
    drawAllAgain();
};

function selectRiskRelated() {
    assetIsActive = false;
    systemAssetIsActive = false;
    businessAssetIsActive = false;
    securityCriterionIsActive = false;
    riskIsActive = true;
    impactIsActive = true;
    eventIsActive = true;
    attackMethodIsActive = true;
    vulnerabilityIsActive = true;
    threatAgentIsActive = true;
    threatIsActive = true;
    riskTreatmentIsActive = false;
    securityRequirementIsActive = false;
    controlIsActive = false;
    var active = ["Risk", "Impact", "Event", "Attack Method", "Vulnerability", "Threat", "Threat Agent"];
    for (var i = 0; i < active.length; i++) {
        syncDropDowns();
    }
    var inActive = ["System Asset", "Business Asset", "Security Criterion", "Risk Treatment", "Security Requirement", "Control"];
    for (var i = 0; i < inActive.length; i++) {
        syncDropDowns();
    }
    drawAllAgain();
};

function selectRiskTreatmentRelated() {
    assetIsActive = false;
    systemAssetIsActive = false;
    businessAssetIsActive = false;
    securityCriterionIsActive = false;
    riskIsActive = false;
    impactIsActive = false;
    eventIsActive = false;
    attackMethodIsActive = false;
    vulnerabilityIsActive = false;
    threatAgentIsActive = false;
    threatIsActive = false;
    riskTreatmentIsActive = true;
    securityRequirementIsActive = true;
    controlIsActive = true;
    var active = ["Risk Treatment", "Security Requirement", "Control"];
    for (var i = 0; i < active.length; i++) {
        syncDropDowns();
    }
    var inActive = ["System Asset", "Business Asset", "Security Criterion", "Risk", "Impact", "Event", "Attack Method", "Vulnerability", "Threat", "Threat Agent"];
    for (var i = 0; i < inActive.length; i++) {
        syncDropDowns();
    }
    drawAllAgain();
};

function selectAll() {
    assetIsActive = true;
    systemAssetIsActive = true;
    businessAssetIsActive = true;
    securityCriterionIsActive = true;
    riskIsActive = true;
    impactIsActive = true;
    eventIsActive = true;
    attackMethodIsActive = true;
    vulnerabilityIsActive = true;
    threatAgentIsActive = true;
    threatIsActive = true;
    riskTreatmentIsActive = true;
    securityRequirementIsActive = true;
    controlIsActive = true;
    var active = ["System Asset", "Business Asset", "Security Criterion", "Risk", "Impact", "Event", "Attack Method", "Vulnerability", "Threat", "Threat Agent", "Risk Treatment", "Security Requirement", "Control"];
    for (var i = 0; i < active.length; i++) {
        syncDropDowns();
    }
    drawAllAgain();
};

function deselectAll() {
    assetIsActive = false;
    systemAssetIsActive = false;
    businessAssetIsActive = false;
    securityCriterionIsActive = false;
    riskIsActive = false;
    impactIsActive = false;
    eventIsActive = false;
    attackMethodIsActive = false;
    vulnerabilityIsActive = false;
    threatAgentIsActive = false;
    threatIsActive = false;
    riskTreatmentIsActive = false;
    securityRequirementIsActive = false;
    controlIsActive = false;
    var inActive = ["System Asset", "Business Asset", "Security Criterion", "Risk", "Impact", "Event", "Attack Method", "Vulnerability", "Threat", "Threat Agent", "Risk Treatment", "Security Requirement", "Control"];
    for (var i = 0; i < inActive.length; i++) {
        syncDropDowns();
    }
    drawAllAgain();
};

function handleMouseDown(e) {

    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    var canvasOffset = $("#canvas").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    mouseX = parseInt(e.pageX - offsetX);
    mouseY = parseInt(e.pageY - offsetY);

    $(".dropdown").toggle(false);

    var clicked = "";
    for (var i = 0; i < rects.length; i++) {
        if (rects[i].type == "UC" && rects[i].isPointInside(mouseX, mouseY)) {
            clicked = rects[i].id;
        }
    }

    if (clicked.length > 0) {
        var name = "#dropdown" + clicked;
        $(name).toggle(true);
    }
}

function drawAgain() {
    if ($("#canvas").is(":hidden")) {
        return;
    }

    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");
    canvasOffset = $("#canvas").offset();
    offsetX = canvasOffset.left;
    offsetY = canvasOffset.top;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    activeArray = createActiveArray();
    rects.push(new rect("Actor_OR_Misuser", 60, 100, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("Actor", 15, 180, 80, 40, "white", "black", 3, activeArray, "UC"));
    rects.push(new rect("Misuser", 130, 180, 80, 40, "white", "black", 3, activeArray, "UC"));
    rects.push(new rect("CommunicationUC", 250, 100, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("IncludeUC", 350, 5, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("ExtendUC", 550, 5, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("Use_OR_Misuse_Case", 450, 100, 80, 40, "white", "black", 1, activeArray, "UC"));

    rects.push(new rect("Constraint_OfUC", 150, 250, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("UseCase", 320, 200, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("Vulnerability", 450, 200, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("ExploitUC", 600, 200, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("SecurityCriterion", 150, 350, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("NegateUC", 150, 450, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("SecurityUseCase", 350, 350, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("ThreatenUC", 430, 300, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("HarmUC", 320, 400, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("MitigateUC", 500, 350, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("Impact", 500, 450, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("MisUseCase", 700, 300, 80, 40, "white", "black", 1, activeArray, "UC"));
    rects.push(new rect("Lead_ToUC", 700, 450, 80, 40, "white", "black", 1, activeArray, "UC"));
    drawLines();
    drawText();
    drawArrows();
};

function setActiveState(opselected, checked) {
    if (opselected === "Business Asset") {
        businessAssetIsActive = checked;
    }
    if (opselected === "System Asset") {
        systemAssetIsActive = checked;
    }
    if (opselected === "Security Criterion") {
        securityCriterionIsActive = checked;
    }
    if (opselected === "Risk") {
        riskIsActive = checked;
        eventIsActive = checked;
        impactIsActive = checked;
        threatIsActive = checked;
        vulnerabilityIsActive = checked;
        threatAgentIsActive = checked;
        attackMethodIsActive = checked;
    }
    if (opselected === "Event") {
        eventIsActive = checked;
        if (checked) {
            threatIsActive = true;
            vulnerabilityIsActive = true;
            threatAgentIsActive = true;
            attackMethodIsActive = true;
            if (impactIsActive) {
                riskIsActive = true;
            }
        } else {
            riskIsActive = false;
            threatIsActive = false;
            vulnerabilityIsActive = false;
            threatAgentIsActive = false;
            attackMethodIsActive = false;
        }
    }
    if (opselected === "Impact") {
        impactIsActive = checked;
        if (checked) {
            if (eventIsActive) {
                riskIsActive = true;
            }
        } else {
            riskIsActive = false;
        }
    }
    if (opselected === "Threat") {
        threatIsActive = checked;
        if (checked) {
            threatAgentIsActive = true;
            attackMethodIsActive = true;
            if (vulnerabilityIsActive && impactIsActive) {
                eventIsActive = true;
                riskIsActive = true;
            } else if (vulnerabilityIsActive) {
                eventIsActive = true;
            }
        } else {
            threatAgentIsActive = false;
            attackMethodIsActive = false;
            if (vulnerabilityIsActive && impactIsActive) {
                eventIsActive = false;
                riskIsActive = false;
            }
            else if (vulnerabilityIsActive) {
                eventIsActive = false;
            }
        }
    }
    if (opselected === "Vulnerability") {
        vulnerabilityIsActive = checked;
        if (checked) {
            if (threatIsActive && impactIsActive) {
                eventIsActive = true;
                riskIsActive = true;
            } else if (threatIsActive) {
                eventIsActive = true;
            }
        } else {
            eventIsActive = false;
            riskIsActive = false;
        }
    }
    if (opselected === "Threat Agent") {
        threatAgentIsActive = checked;
        if (checked) {
            if (attackMethodIsActive && vulnerabilityIsActive && impactIsActive) {
                riskIsActive = true;
                eventIsActive = true;
                threatIsActive = true;
            } else if (attackMethodIsActive && vulnerabilityIsActive) {
                threatIsActive = true;
                eventIsActive = true;
            } else if (attackMethodIsActive) {
                threatIsActive = true;
            }
        } else {
            threatIsActive = false;
            eventIsActive = false;
            riskIsActive = false;
        }
    }
    else if (opselected === "Attack Method") {
        attackMethodIsActive = checked;
        if (checked) {
            if (threatAgentIsActive && vulnerabilityIsActive && impactIsActive) {
                riskIsActive = true;
                eventIsActive = true;
                threatIsActive = true;
            }
            else if (threatAgentIsActive && vulnerabilityIsActive) {
                threatIsActive = true;
                eventIsActive = true;
            }
            else if (threatAgentIsActive) {
                threatIsActive = true;
            }
        } else {
            threatIsActive = false;
            eventIsActive = false;
            riskIsActive = false;
        }
    }
    if (opselected === "Security Requirement") {
        securityRequirementIsActive = checked;
    }
    if (opselected === "Control") {
        controlIsActive = checked;
    }
}

function toggleUseCases() {

    $("#canvas").toggle();
    $("#usecasetext").toggle();

    if ($('#ucbutton').hasClass("btn-success")) {
        $('#ucbutton').removeClass("btn-success");
        $('#ucbutton').addClass("btn-danger");
    } else {
        $('#ucbutton').removeClass("btn-danger");
        $('#ucbutton').addClass("btn-success");
    }

    drawAgain();
    syncTable();
    drawLegend();
    $("#canvas").click(handleMouseDown);
}

function drawAllAgain() {
    drawAgain();
    drawMalAgain();
    drawTroposAgain();
    drawBPMNAgain();
    syncTable();
    drawLegend();
}


$(function () {
    $('#selectboxActor').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownActor").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxMisuser').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMisuser").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSecurityCriterion').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecurityCriterion").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxUseCase').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownUseCase").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxVulnerability').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownVulnerability").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxMisUseCase').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMisUseCase").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxImpact').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownImpact").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxSecurityUseCase').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecurityUseCase").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxExtendUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownExtendUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxIncludeUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownIncludeUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxConstraint_OfUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownConstraint_OfUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxCommunicationUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownCommunicationUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxExploitUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownExploitUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxThreatenUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownThreatenUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxLead_ToUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownLead_ToUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxHarmUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownHarmUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxNegateUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownNegateUC").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxMitigateUC').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMitigateUC").toggle(false);
            drawAllAgain();
        }
    });
});