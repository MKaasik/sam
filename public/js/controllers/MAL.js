function drawMalText() {
    var canvas = document.getElementById("MALcanvas1");
    var ctx = canvas.getContext("2d");

    ctx.font = "italic 30px Arial";
    ctx.fillStyle = "black";
    ctx.fillText("Mal Activity diagrams", 150, 50);

    ctx.font = "italic 10px Arial";
    ctx.fillText("AnyState", 80, 120);
    ctx.font = "10px Arial";

    ctx.fillText("InitialState", 20, 200);
    ctx.fillText("FinalState", 140, 200);

    ctx.font = "italic 10px Arial";

    ctx.fillText("AnySwimLane", 410, 120);
    ctx.font = "10px Arial";
    ctx.fillText("Swimlane", 260, 200);
    ctx.fillText("Mal-Swimlane", 560, 200);

    ctx.font = "italic 10px Arial";
    ctx.fillText("SwimLaneElement", 245, 320);
    ctx.fillText("Mal-SwimLaneElement", 530, 320);

    ctx.fillText("AnyActivity", 160, 420);
    ctx.fillText("AnyDecision", 445, 420);
    ctx.font = "10px Arial";
    ctx.fillText("Activity", 60, 500);
    ctx.fillText("Mal-Activity", 160, 500);
    ctx.fillText("MitigationActivity", 240, 500);

    ctx.fillText("Decision", 360, 500);
    ctx.fillText("Mal-Decision", 550, 500);

    //relationship labels

    ctx.fillText("Includes", 210, 105);
    ctx.fillText("Includes", 250, 260);
    ctx.fillText("Includes", 550, 265);

    //Second Diagram
    ctx.fillText("InitialState", 120, 620);
    ctx.fillText("FinalState", 420, 620);
    ctx.fillText("MitigationLink", 30, 720);
    ctx.fillText("ControlFlow", 260, 720);
    ctx.font = "italic 10px Arial";
    ctx.fillText("AnyActivity", 120, 820);
    ctx.fillText("AnyDecision", 420, 780);
    ctx.font = "10px Arial";
    ctx.fillText("Vulnerability", 410, 860);
    ctx.fillText("MitigationActivity", 20, 920);
    ctx.fillText("Mal-Activity", 130, 920);
    ctx.fillText("Activity", 240, 920);
    ctx.fillText("SecurityCriterion", 400, 920);

    //relationship labels

    ctx.fillText("from", 210, 610);
    ctx.fillText("to", 340, 610);
    ctx.fillText("to", 170, 710);
    ctx.fillText("to", 360, 710);
    ctx.fillText("from", 20, 760);
    ctx.fillText("from", 150, 760);
    ctx.fillText("to", 240, 760);
    ctx.fillText("from", 330, 760);
    ctx.fillText("characteristic of", 330, 850);
    ctx.fillText("constraint of", 330, 910);
}

function drawMalLines() {
    canvas = document.getElementById("MALcanvas1");
    ctx = canvas.getContext("2d");

    ctx.moveTo(55, 180);
    ctx.lineTo(55, 160);
    ctx.lineTo(170, 160);
    ctx.lineTo(170, 180);
    ctx.moveTo(100, 160);
    ctx.lineTo(100, 140);
    ctx.moveTo(140, 120);
    ctx.lineTo(400, 120);

    ctx.moveTo(440, 140);
    ctx.lineTo(440, 160);
    ctx.lineTo(290, 160);
    ctx.lineTo(290, 180);
    ctx.moveTo(440, 160);
    ctx.lineTo(590, 160);
    ctx.lineTo(590, 180);

    ctx.moveTo(290, 220);
    ctx.lineTo(290, 300);

    ctx.moveTo(290, 340);
    ctx.lineTo(290, 360);
    ctx.lineTo(190, 360);
    ctx.lineTo(190, 400);

    ctx.moveTo(290, 360);
    ctx.lineTo(290, 480);
    ctx.moveTo(290, 360);
    ctx.lineTo(380, 360);
    ctx.lineTo(380, 480);


    ctx.moveTo(190, 440);
    ctx.lineTo(190, 480);
    ctx.moveTo(190, 460);
    ctx.lineTo(90, 460);
    ctx.lineTo(90, 480);

    ctx.moveTo(190, 460);
    ctx.lineTo(280, 460);
    ctx.lineTo(280, 480);

    ctx.moveTo(480, 440);
    ctx.lineTo(480, 460);
    ctx.lineTo(400, 460);
    ctx.lineTo(400, 480);

    ctx.moveTo(480, 460);
    ctx.lineTo(580, 460);
    ctx.lineTo(580, 480);

    ctx.moveTo(610, 340);
    ctx.lineTo(610, 480);

    ctx.moveTo(610, 360);
    ctx.lineTo(650, 360);
    ctx.lineTo(650, 540);
    ctx.lineTo(180, 540);
    ctx.lineTo(180, 520);

    ctx.moveTo(590, 300);
    ctx.lineTo(590, 220);

    //SECOND Diagram

    ctx.moveTo(180, 620);
    ctx.lineTo(260, 620);
    ctx.lineTo(260, 700);

    ctx.moveTo(400, 620);
    ctx.lineTo(300, 620);
    ctx.lineTo(300, 700);

    ctx.moveTo(100, 720);
    ctx.lineTo(240, 720);

    ctx.moveTo(240, 730);
    ctx.lineTo(140, 730);
    ctx.lineTo(140, 800);

    ctx.moveTo(260, 740);
    ctx.lineTo(260, 820);
    ctx.lineTo(180, 820);

    ctx.moveTo(60, 740);
    ctx.lineTo(60, 900);

    ctx.moveTo(80, 900);
    ctx.lineTo(80, 860);
    ctx.lineTo(260, 860);
    ctx.lineTo(260, 900);

    ctx.moveTo(140, 900);
    ctx.lineTo(140, 840);

    ctx.moveTo(300, 920);
    ctx.lineTo(400, 920);

    ctx.moveTo(280, 740);
    ctx.lineTo(280, 860);
    ctx.lineTo(400, 860);

    ctx.moveTo(300, 740);
    ctx.lineTo(300, 780);
    ctx.lineTo(400, 780);

    ctx.moveTo(320, 720);
    ctx.lineTo(440, 720);
    ctx.lineTo(440, 760);

    ctx.stroke();
};

function drawMalArrows() {
    drawArrow("MALcanvas1", 100, 140, "UP", "white");
    drawArrow("MALcanvas1", 200, 100, "LEFT", "black");
    drawArrow("MALcanvas1", 440, 140, "UP", "white");

    drawArrow("MALcanvas1", 280, 280, "DOWN", "black");
    drawArrow("MALcanvas1", 580, 280, "DOWN", "black");

    drawArrow("MALcanvas1", 290, 340, "UP", "white");
    drawArrow("MALcanvas1", 610, 340, "UP", "white");

    drawArrow("MALcanvas1", 190, 440, "UP", "white");
    drawArrow("MALcanvas1", 480, 440, "UP", "white");

    //    Second diagram

    drawArrow("MALcanvas1", 200, 610, "LEFT", "black");
    drawArrow("MALcanvas1", 360, 610, "RIGHT", "black");
    drawArrow("MALcanvas1", 200, 710, "RIGHT", "black");
    drawArrow("MALcanvas1", 400, 710, "RIGHT", "black");

    drawArrow("MALcanvas1", 40, 780, "DOWN", "black");
    drawArrow("MALcanvas1", 150, 780, "DOWN", "black");
    drawArrow("MALcanvas1", 250, 780, "DOWN", "black");
    drawArrow("MALcanvas1", 380, 760, "RIGHT", "black");

    drawArrow("MALcanvas1", 320, 850, "LEFT", "black");
    drawArrow("MALcanvas1", 320, 910, "LEFT", "black");
    drawArrow("MALcanvas1", 140, 840, "UP", "white");

};

function handleMALMouseDown(e) {

    var canvas = document.getElementById("MALcanvas1");
    var ctx = canvas.getContext("2d");
    var canvasOffset = $("#MALcanvas1").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    mouseX = parseInt(e.pageX - offsetX);
    mouseY = parseInt(e.pageY - offsetY);

    $(".dropdown").toggle(false);

    var clicked = "";
    for (var i = 0; i < rects.length; i++) {
        if (rects[i].type == "MAL" && rects[i].isPointInside(mouseX, mouseY)) {
            clicked = rects[i].id;
        }
    }
    if (clicked.length > 0) {
        var name = "#dropdown" + clicked;
        $(name).toggle(true);
    }
}

function drawMalAgain() {
    if ($("#MALcanvas1").is(":hidden")) {
        return;
    }

    canvas = document.getElementById("MALcanvas1");
    ctx = canvas.getContext("2d");
    canvasOffset = $("#MALcanvas1").offset();
    offsetX = canvasOffset.left;
    offsetY = canvasOffset.top;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    var activeArray = createActiveArray();
    rects.push(new rect("AnyState-MAL", 60, 100, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("InitialState-MAL", 15, 180, 80, 40, "white", "black", 3, activeArray, "MAL"));
    rects.push(new rect("FinalState-MAL", 130, 180, 80, 40, "white", "black", 3, activeArray, "MAL"));
    rects.push(new rect("SwimLane-MAL", 250, 180, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MalSwimLane-MAL", 550, 180, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("AnySwimLane-MAL", 400, 100, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("SwimLaneElement-MAL", 250, 300, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MalSwimLaneElement-MAL", 550, 300, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("AnyActivity-MAL", 150, 400, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("Activity-MAL1", 50, 480, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MalActivity-MAL1", 140, 480, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MitigationActivity-MAL1", 240, 480, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("AnyDecision-MAL", 440, 400, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("Decision-MAL", 340, 480, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MalDecision-MAL", 540, 480, 80, 40, "white", "black", 1, activeArray, "MAL"));

    //SECOND Diagram

    rects.push(new rect("InitialState-MAL", 100, 600, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("FinalState-MAL", 400, 600, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MitigationLink-MAL", 20, 700, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("ControlFlow-MAL", 240, 700, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("AnyActivity-MAL", 100, 800, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("AnyDecision-MAL", 400, 760, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("Vulnerability-MAL", 400, 840, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MitigationActivity-MAL2", 20, 900, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("MalActivity-MAL2", 120, 900, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("Activity-MAL2", 220, 900, 80, 40, "white", "black", 1, activeArray, "MAL"));
    rects.push(new rect("SecurityCriterion-MAL", 400, 900, 80, 40, "white", "black", 1, activeArray, "MAL"));

    drawMalLines();
    drawMalText();
    drawMalArrows();
};

function toggleMal() {

    if ($('#malbutton').hasClass("btn-success")) {
        $('#malbutton').removeClass("btn-success");
        $('#malbutton').addClass("btn-danger");
    } else {
        $('#malbutton').removeClass("btn-danger");
        $('#malbutton').addClass("btn-success");
    }

    $("#MALcanvas1").toggle();
    $("#maltext").toggle();
    drawMalAgain();
    syncTable();
    drawLegend();
    $("#MALcanvas1").click(handleMALMouseDown);
}
$(function(){
    $('#selectboxActivityMal1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownActivity-MAL1").toggle(false);
            drawAllAgain();

        }
    });
    $('#selectboxActivityMal2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownActivity-MAL2").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDecisionMal').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();

            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDecision-MAL").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxControlFlowMal').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();

            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownControlFlow-MAL").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxSwimLaneMal').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();

            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSwimLane-MAL").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxMalActivity1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMalActivity-MAL1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxMalActivity2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();

            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMalActivity-MAL2").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxMalSwimlane').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();

            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMalSwimLane-MAL").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxMalDecision').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();

            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMalDecision-MAL").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxMitigationActivityMAL1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMitigationActivity-MAL1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxMitigationActivityMAL2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMitigationActivity-MAL2").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxSecurityCriterion-MAL').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecurityCriterion-MAL").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxVulnerability-MAL').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownVulnerability-MAL").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxMitigationLink-MAL').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMitigationLink-MAL").toggle(false);
            drawAllAgain();
        }
    });
});