var troposTD;

function syncTable() {
    hideAllIcons();

    bpmnActive = false;
    troposActive = false;
    usecaseActive = false;
    activityDiagramActive = false;

    var activeSet = new Set();

    if ($("#BPMNcanvas").is(":visible")) {
        bpmnActive = true;
    }
    if ($("#TROPOScanvas").is(":visible")) {
        troposActive = true;
    }
    if ($("#canvas").is(":visible")) {
        usecaseActive = true;
    }
    if ($("#MALcanvas1").is(":visible")) {
        activityDiagramActive = true;
    }

    if (bpmnActive) {
        $("#mainRow").append(bpmnTableHTML);
    }
    if (troposActive) {
        $("#mainRow").append(troposHTML);
    }
    if (usecaseActive) {
        $("#mainRow").append(useCaseHTML);
    }
    if (activityDiagramActive) {
        $("#mainRow").append(activityDiagramHTML);
    }

    if (businessAssetIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnGatewayTR");
            activeSet.add("#bpmnBTaskTR");
            activeSet.add("#bpmnSequenceFlowTR");
            activeSet.add("#bpmnDataObjectTR");
        }
        if (troposActive) {
            activeSet.add("#troposActorTR");
            activeSet.add("#troposhardgoalTR");
            activeSet.add("#troposSoftGoalTR");
            activeSet.add("#troposplanTR");
            activeSet.add("#troposResourceTR");
            activeSet.add("#troposDependencyTR");
            activeSet.add("#troposMeansEndTR");
            activeSet.add("#troposContributionTR");
            activeSet.add("#troposDecompostionTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseUCTR");
            activeSet.add("#usecaseActorTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityActivityTR");
            activeSet.add("#activityDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activitySwimLaneTR");
            activeSet.add("#activityInitialNodeTR");
            activeSet.add("#activityFinalNodeTR");
        }
    }
    if (systemAssetIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnGatewayTR");
            activeSet.add("#bpmnSTaskTR");
            activeSet.add("#bpmnSequenceFlowTR");
            activeSet.add("#bpmnPoolTR");
            activeSet.add("#bpmnLaneTR");
            activeSet.add("#bpmnDataStoreTR");
        }
        if (troposActive) {
            activeSet.add("#troposActorTR");
            activeSet.add("#troposhardgoalTR");
            activeSet.add("#troposSoftGoalTR");
            activeSet.add("#troposplanTR");
            activeSet.add("#troposResourceTR");
            activeSet.add("#troposDependencyTR");
            activeSet.add("#troposMeansEndTR");
            activeSet.add("#troposContributionTR");
            activeSet.add("#troposDecompostionTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseUCTR");
            activeSet.add("#usecaseActorTR");
            activeSet.add("#usecaseSystemBoundaryTR");
            activeSet.add("#usecaseExtendTR");
            activeSet.add("#usecaseIncludeTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activitySwimLaneTR");
            activeSet.add("#activityActivityTR");
            activeSet.add("#activityDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activityInitialNodeTR");
            activeSet.add("#activityFinalNodeTR");
        }
    }
    if (securityCriterionIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnAnnotationTR");
            activeSet.add("#bpmnLockedTR");
        }
        if (troposActive) {
            activeSet.add("#troposSoftGoalTR");
            activeSet.add("#troposSecurityConstraintTR");
            activeSet.add("#troposContributionTR");
            activeSet.add("#troposDependencyTR");
            activeSet.add("#troposDecompostionTR");
            activeSet.add("#troposRestrictsTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseSecurityCriterionTR");
            activeSet.add("#usecaseConstraintTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityCommentTR");
        }
    }
    if (attackMethodIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnGatewayTR");
            activeSet.add("#bpmnRedTaskTR");
            activeSet.add("#bpmnSequenceFlowTR");
            activeSet.add("#bpmnRedDataFlowTR");
        }
        if (troposActive) {
            activeSet.add("#troposplanTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseMUCTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMalActivityTR");
            activeSet.add("#activityMalDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activityMalSwimLaneTR");
            activeSet.add("#activityInitialNodeTR");
            activeSet.add("#activityFinalNodeTR");
        }
    }
    if (threatAgentIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnRedPoolTR");
            activeSet.add("#bpmnRedLaneTR");
        }
        if (troposActive) {
            activeSet.add("#troposattackerTR");

        }
        if (usecaseActive) {
            activeSet.add("#usecaseMisuserTR");
            activeSet.add("#usecaseCommunicationTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMalSwimLaneTR");
        }
    }
    if (vulnerabilityIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnRedAnnotationTR");
        }
        if (troposActive) {
            activeSet.add("#troposVulnerabilityTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseVulnerabilityTR");
            activeSet.add("#usecaseExtendTR");
            activeSet.add("#usecaseIncludeTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityVulnerabilityTR");
        }
    }
    if (threatIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnGatewayTR");
            activeSet.add("#bpmnredTaskTR");
            activeSet.add("#bpmnSequenceFlowTR");
            activeSet.add("#bpmnRedDataFlowTR");
            activeSet.add("#bpmnRedPoolTR");
            activeSet.add("#bpmnRedLaneTR");
        }
        if (troposActive) {
            activeSet.add("#troposattackerTR");
            activeSet.add("#troposplanTR");
            activeSet.add("#troposexploitsTR");
            activeSet.add("#troposAttacksTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseMUCTR");
            activeSet.add("#usecaseMisuserTR");
            activeSet.add("#CommunicationUC");
            activeSet.add("#usecaseExploitsTR");
            activeSet.add("#usecaseThreatensTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMalActivityTR");
            activeSet.add("#activityMalDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activityMalSwimLaneTR");
        }
    }
    if (impactIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnUnlockedTR");
        }
        if (troposActive) {
            activeSet.add("#troposImpactTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseImpactTR");
            activeSet.add("#usecaseHarmTR");
            activeSet.add("#usecaseNegateTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMalActivityTR");
        }
    }
    if (eventIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnGatewayTR");
            activeSet.add("#bpmnredTaskTR");
            activeSet.add("#bpmnSequenceFlowTR");
            activeSet.add("#bpmnRedDataFlowTR");
            activeSet.add("#bpmnRedPoolTR");
            activeSet.add("#bpmnRedLaneTR");
            activeSet.add("#bpmnRedAnnotationTR");
        }
        if (troposActive) {
            activeSet.add("#troposhardgoalTR");
            activeSet.add("#troposplanTR");
            activeSet.add("#troposexploitsTR");
            activeSet.add("#troposVulnerabilityTR");
            activeSet.add("#troposThreatTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseMisuserTR");
            activeSet.add("#CommunicationUC");
            activeSet.add("#usecaseMUCTR");
            activeSet.add("#usecaseVulnerabilityTR");
            activeSet.add("#usecaseExtendTR");
            activeSet.add("#usecaseIncludeTR");
            activeSet.add("#usecaseLeadToTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMalActivityTR");
            activeSet.add("#activityMalDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activityMalSwimLaneTR");
        }
    }
    if (riskIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnGatewayTR");
            activeSet.add("#bpmnredTaskTR");
            activeSet.add("#bpmnSequenceFlowTR");
            activeSet.add("#bpmnRedDataFlowTR");
            activeSet.add("#bpmnRedPoolTR");
            activeSet.add("#bpmnRedLaneTR");
            activeSet.add("#bpmnRedAnnotationTR");
        }
        if (troposActive) {
            activeSet.add("#troposhardgoalTR");
            activeSet.add("#troposplanTR");
            activeSet.add("#troposexploitsTR");
            activeSet.add("#troposAttacksTR");
            activeSet.add("#troposVulnerabilityTR");
            activeSet.add("#troposImpactTR");
            activeSet.add("#troposThreatTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseMisuserTR");
            activeSet.add("#CommunicationUC");
            activeSet.add("#usecaseMUCTR");
            activeSet.add("#usecaseVulnerabilityTR");
            activeSet.add("#usecaseImpactTR");
            activeSet.add("#usecaseExtendTR");
            activeSet.add("#usecaseIncludeTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMalActivityTR");
            activeSet.add("#activityMalDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activityMalSwimLaneTR");
        }
    }
    if (riskTreatmentIsActive) {

    }
    if (securityRequirementIsActive) {
        if (bpmnActive) {
            activeSet.add("#bpmnEventTR");
            activeSet.add("#bpmnBlueGatewayTR");
            activeSet.add("#bpmnBlueTaskTR");
            activeSet.add("#bpmnBlueSequenceFlowTR");
        }
        if (troposActive) {
            activeSet.add("#troposActorDottedTR");
            activeSet.add("#troposhardgoalDottedTR");
            activeSet.add("#troposplanDottedTR");
            activeSet.add("#troposResourceDottedTR");
            activeSet.add("#troposSoftGoalDottedTR");
            activeSet.add("#troposSecureGoalDottedTR");
            activeSet.add("#troposSecurityConstraintDottedTR");
            activeSet.add("#troposDependencyTR");
            activeSet.add("#troposMeansEndTR");
            activeSet.add("#troposContributionTR");
            activeSet.add("#troposDecompostionTR");
            activeSet.add("#troposMitigatesTR");
            activeSet.add("#troposSatisfiesTR");
        }
        if (usecaseActive) {
            activeSet.add("#usecaseSecurityUCTR");
            activeSet.add("#usecaseMitigatesTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activityMitigationActivityTR");
            activeSet.add("#activityMitigationDecisionTR");
            activeSet.add("#activityControlFlowTR");
            activeSet.add("#activityMitigationLinkTR");
            activeSet.add("#activityInitialNodeTR");
            activeSet.add("#activityFinalNodeTR");
        }
    }
    if (controlIsActive) {
        if (troposActive) {
            activeSet.add("#troposActorDottedTR");
            activeSet.add("#troposhardgoalDottedTR");
            activeSet.add("#troposplanDottedTR");
            activeSet.add("#troposResourceDottedTR");
            activeSet.add("#troposSoftGoalDottedTR");
            activeSet.add("#troposSecureGoalDottedTR");
            activeSet.add("#troposSecurityConstraintDottedTR");
            activeSet.add("#troposDependencyTR");
            activeSet.add("#troposMeansEndTR");
            activeSet.add("#troposContributionTR");
            activeSet.add("#troposDecompostionTR");
            activeSet.add("#troposSatisfiesTR");
        }
        if (activityDiagramActive) {
            activeSet.add("#activitySwimLaneTR");
        }
    }

    activeSet.forEach(function (item) {
        show($(item));
    });

}

function show(element) {
    element.removeClass("hidden");
}

$(function () {
    syncTable();
});

function hideAllIcons() {
    // $("#mainRow tr").each(function (i, object) {
    //     $(object).addClass("hidden");
    // });
    $("#mainRow").empty();

}

var bpmnTableHTML = '<td valign="top" id="bpmnTD">\n' +
    '      <table id="bpmnTable">\n' +
    '            <thead>\n' +
    '                  <tr>\n' +
    '                  <th id="bpmnheader">BPMN Elements</th>\n' +
    '                  </tr>\n' +
    '            </thead>\n' +
    '            <tbody>\n' +
    '                  <tr class="hidden" id="bpmnPoolTR">\n' +
    '                        <td>Pool <img src="../img/bpmn/pool.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnLaneTR">\n' +
    '                        <td>Lane <img src="../img/bpmn/lane.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnRedPoolTR">\n' +
    '                        <td>Pool <img src="../img/bpmn/redpool.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnRedLaneTR">\n' +
    '                        <td>Lane <img src="../img/bpmn/redlane.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnBTaskTR">\n' +
    '                        <td>Task <img src="../img/bpmn/btask.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnSTaskTR">\n' +
    '                        <td>Task <img src="../img/bpmn/stask.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnRedTaskTR">\n' +
    '                        <td>Task <img src="../img/bpmn/redtask.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnBlueTaskTR">\n' +
    '                        <td>Task <img src="../img/bpmn/bluetask.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnEventTR">\n' +
    '                        <td>Event <img src="../img/bpmn/event.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnDataObjectTR">\n' +
    '                        <td>Data Object <img src="../img/bpmn/dataobject.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnDataStoreTR">\n' +
    '                        <td>Data Store <img src="../img/bpmn/datastore.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnLockedTR">\n' +
    '                        <td>Lock <img src="../img/bpmn/locked.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnUnlockedTR">\n' +
    '                        <td>Lock <img src="../img/bpmn/unlocked.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnAnnotationTR">\n' +
    '                        <td>Annotation <img src="../img/bpmn/annotation.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnRedAnnotationTR">\n' +
    '                        <td>Annotation <img src="../img/bpmn/redannotation.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnSequenceFlowTR">\n' +
    '                        <td>Sequence Flow<img src="../img/bpmn/sequenceflow.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnBlueSequenceFlowTR">\n' +
    '                        <td>Sequence Flow<img src="../img/bpmn/bluesequenceflow.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnGatewayTR">\n' +
    '                        <td>Gateway <img src="../img/bpmn/gateway.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnBlueGatewayTR">\n' +
    '                        <td>Gateway <img src="../img/bpmn/bluegateway.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnDataFlowTR">\n' +
    '                        <td>Data Flow <img src="../img/bpmn/dataFlow.png"></td>\n' +
    '                  </tr>\n' +
    '                  <tr class="hidden" id="bpmnRedDataFlowTR">\n' +
    '                        <td>Data Flow <img src="../img/bpmn/reddataflow.png"></td>\n' +
    '                  </tr>\n' +
    '            </tbody>\n' +
    '      </table>\n' +
    '</td>';

var troposHTML = ' <td valign="top" id="troposTD">\n' +
    '                        <table id="troposTable">\n' +
    '                            <thead>\n' +
    '                            <tr>\n' +
    '                                <th id="troposheader">TROPOS Elements</th>\n' +
    '                            </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                            <tr class="hidden" id="troposActorTR">\n' +
    '                                <td>Actor <img src="../img/tropos/actor.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposattackerTR">\n' +
    '                               <td>Attacker <img src="../img/tropos/attacker.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposplanTR">\n' +
    '                                <td>Plan <img src="../img/tropos/plan.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposhardgoalTR">\n' +
    '                                <td>Hard Goal <img src="../img/tropos/hardgoal.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposResourceTR">\n' +
    '                                <td>Resource <img src="../img/tropos/resource.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSecureGoalTR">\n' +
    '                                <td>Secure Goal <img src="../img/tropos/securegoal.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSoftGoalTR">\n' +
    '                                <td>Soft Goal<img src="../img/tropos/softgoal.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSecurityConstraintTR">\n' +
    '                               <td>Security Constraint <img src="../img/tropos/securityconstraint.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposActorDottedTR">\n' +
    '                               <td>Actor <img src="../img/tropos/actor_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposhardgoalDottedTR">\n' +
    '                               <td>Hard Goal <img src="../img/tropos/hardgoal_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposplanDottedTR">\n' +
    '                               <td>Plan <img src="../img/tropos/plan_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposResourceDottedTR">\n' +
    '                               <td>Resource <img src="../img/tropos/resource_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSoftGoalDottedTR">\n' +
    '                               <td>Soft Goal<img src="../img/tropos/softgoal_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSecureGoalDottedTR">\n' +
    '                               <td>Secure Goal<img src="../img/tropos/securegoal_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSecurityConstraintDottedTR">\n' +
    '                               <td>Security Constraint<img src="../img/tropos/securityconstraint_dotted.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposDependencyTR">\n' +
    '                                <td>Dependency relationship<img src="../img/tropos/dependency.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposMeansEndTR">\n' +
    '                                <td>Means-End relationship<img src="../img/tropos/meansend.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposContributionTR">\n' +
    '                                <td>Contribution relationship<img src="../img/tropos/contribution.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposDecompostionTR">\n' +
    '                                <td>Decomposition relationship<img src="../img/tropos/decomposition.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposRestrictsTR">\n' +
    '                                <td>restricts relationship <img src="../img/tropos/restricts.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposMitigatesTR">\n' +
    '                                <td>Mitigates relationship <img src="../img/tropos/mitigates.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposSatisfiesTR">\n' +
    '                                <td>Satisifies relationship <img src="../img/tropos/satisfies.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposVulnerabilityTR">\n' +
    '                               <td>Vulnerability Point <img src="../img/tropos/vulnerability.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposImpactTR">\n' +
    '                               <td>Impacts relationship <img src="../img/tropos/impacts.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposThreatTR">\n' +
    '                               <td>Threat <img src="../img/tropos/threat.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposexploitsTR">\n' +
    '                               <td>Exploits relationship <img src="../img/tropos/exploits.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="troposAttacksTR">\n' +
    '                               <td>Attacks relationship <img src="../img/tropos/attacks.png"></td>\n' +
    '                            </tr>\n' +
    '                         </tbody>\n' +
    '                        </table>\n' +
    '                    </td';


var useCaseHTML = '<td valign="top" id="usecaseTD">\n' +
    '                        <table id="usecaseTable">\n' +
    '                            <thead>\n' +
    '                            <tr>\n' +
    '                                <th id="usecaseheader">Use Case Elements</th>\n' +
    '                            </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                            <tr class="hidden" id="usecaseActorTR">\n' +
    '                                <td>Actor <img src="../img/usecase/actor.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseMisuserTR">\n' +
    '                                <td>Misuser <img src="../img/usecase/misuser.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseUCTR">\n' +
    '                                <td>Use Case <img src="../img/usecase/usecase.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseMUCTR">\n' +
    '                                <td>Misuse Case <img src="../img/usecase/misusecase.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseSecurityUCTR">\n' +
    '                                <td>Security Usecase <img src="../img/usecase/securityusecase.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseSystemBoundaryTR">\n' +
    '                                <td>System Boundary <img src="../img/usecase/systemboundary.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseSecurityCriterionTR">\n' +
    '                                <td>Security Criterion <img src="../img/usecase/securitycriterion.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseConstraintTR">\n' +
    '                                <td>Constraint of realtionship <img src="../img/usecase/constraint.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseVulnerabilityTR">\n' +
    '                                <td>Vulnerability <img src="../img/usecase/vulnerability.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseImpactTR">\n' +
    '                                <td>Impact <img src="../img/usecase/impact.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseExtendTR">\n' +
    '                                <td>Extends relationship <img src="../img/usecase/extend.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseIncludeTR">\n' +
    '                                <td>Includes relationship <img src="../img/usecase/include.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseExploitsTR">\n' +
    '                                <td>Exploit link <img src="../img/usecase/exploit.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseThreatensTR">\n' +
    '                                <td>Threaten link <img src="../img/usecase/threaten.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseLeadToTR">\n' +
    '                                <td>Lead to link <img src="../img/usecase/leadto.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseHarmTR">\n' +
    '                                <td>Harms link <img src="../img/usecase/harm.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseNegateTR">\n' +
    '                                <td>Negate link <img src="../img/usecase/negates.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseMitigatesTR">\n' +
    '                                <td>Mitigate link <img src="../img/usecase/mitigate.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="usecaseCommunicationTR">\n' +
    '                                <td>Communication link <img src="../img/usecase/communication.png"></td>\n' +
    '                            </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                    </td>';

var activityDiagramHTML = '   <td valign="top" id="activityTD">\n' +
    '                        <table id="activityTable">\n' +
    '                            <thead>\n' +
    '                            <tr>\n' +
    '                                <th id="activityheader">Activity Diagram Elements</th>\n' +
    '                            </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                            <tr class="hidden" id="activitySwimLaneTR">\n' +
    '                                <td>Swimlane <img src="../img/activity/swimlane.png"></td>\n' +
    '                            </tr>\n' +
    '                           <tr class="hidden" id="activityMalSwimLaneTR">\n' +
    '                                <td>Mal-Swimlane <img src="../img/activity/malswimlane.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityActivityTR">\n' +
    '                                <td>Activity <img src="../img/activity/activity.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityMalActivityTR">\n' +
    '                                <td>Malicious Activity <img src="../img/activity/malactivity.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityMitigationActivityTR">\n' +
    '                                <td>Mitigation Activity <img src="../img/activity/mitigationactivity.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityDecisionTR">\n' +
    '                                <td>Decision <img src="../img/activity/decision.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityInitialNodeTR">\n' +
    '                                <td>Activity Initial State <img src="../img/activity/initialnode.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityFinalNodeTR">\n' +
    '                                <td>Activity final State <img src="../img/activity/finalnode.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityMalDecisionTR">\n' +
    '                                <td>Malicious Decision <img src="../img/activity/maldecision.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityMitigationDecisionTR">\n' +
    '                                <td>Mitigation Decision <img src="../img/activity/mitigationdecision.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityCommentTR">\n' +
    '                                <td>Comment linked to business asset <img src="../img/activity/comment.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityVulnerabilityTR">\n' +
    '                                <td>Comment linked to vulnerabile system assets <img src="../img/activity/vulnerability.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityControlFlowTR">\n' +
    '                                <td>Control flow construct <img src="../img/activity/controlflow.png"></td>\n' +
    '                            </tr>\n' +
    '                            <tr class="hidden" id="activityMitigationLinkTR">\n' +
    '                                <td>Mitigation link <img src="../img/activity/mitigationlink.png"></td>\n' +
    '                            </tr>\n' +
'                            </tbody>\n' +
'                        </table>\n' +
'                    </td>';