function drawLegend() {
    canvas = document.getElementById("legend");
    ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    //Business asset
    ctx.rect(0, 0, 50, 30);
    ctx.fillStyle = "#94DCE5";
    ctx.fill();
    ctx.beginPath();
    //System asset
    ctx.rect(0, 50, 50, 30);
    ctx.fillStyle = "#0039e6";
    ctx.fill();
    ctx.beginPath();

    //SEC-criterion
    ctx.rect(0, 100, 50, 30);
    ctx.fillStyle = "#809fff";
    ctx.fill();
    ctx.beginPath();

    //Risk
    ctx.rect(0, 150, 50, 30);
    ctx.fillStyle = "#ff6666";
    ctx.fill();
    ctx.beginPath();
//Event
    ctx.rect(0, 200, 50, 30);
    ctx.fillStyle = "#ff3333";
    ctx.fill();
    ctx.beginPath();
    //impact
    ctx.rect(0, 250, 50, 30);
    ctx.fillStyle = "#e60000";
    ctx.fill();
    ctx.beginPath();
    //Threat
    ctx.rect(0, 300, 50, 30);
    ctx.fillStyle = "#ffb3b3";
    ctx.fill();
    ctx.beginPath();
    //Vulnerability
    ctx.rect(0, 350, 50, 30);
    ctx.fillStyle = "#ff9999";
    ctx.fill();
    ctx.beginPath();
//Threat Agent
    ctx.beginPath();
    ctx.rect(0, 400, 50, 30);
    ctx.fillStyle = "#e6005c";
    ctx.fill();
    ctx.beginPath();
    //Attack method
    ctx.rect(0, 450, 50, 30);
    ctx.fillStyle = "#ff3385";
    ctx.fill();
    ctx.beginPath();

    // Risk Treatment
    ctx.rect(0, 500, 50, 30);
    ctx.fillStyle = "#85e085";
    ctx.fill();
    ctx.beginPath();
    //Security requirement
    ctx.rect(0, 550, 50, 30);
    ctx.fillStyle = "#1f7a1f";
    ctx.fill();
    ctx.beginPath();
//Control
    ctx.rect(0, 600, 50, 30);
    ctx.fillStyle = "#00ff55";
    ctx.fill();

    ctx.font = "14px Arial";

    ctx.fillStyle = "black";
    if (businessAssetIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Business asset", 80, 20);

    ctx.fillStyle = "black";
    if (systemAssetIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("System Asset", 80, 70);

    ctx.fillStyle = "black";
    if (securityCriterionIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Security Criterion", 80, 120);

    ctx.fillStyle = "black";
    if (riskIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Risk", 80, 170);

    ctx.fillStyle = "black";
    if (eventIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Event", 80, 220);

    ctx.fillStyle = "black";
    if (impactIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Impact", 80, 270);

    ctx.fillStyle = "black";
    if (threatIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Threat", 80, 320);

    ctx.fillStyle = "black";
    if (vulnerabilityIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Vulnerability", 80, 370);

    ctx.fillStyle = "black";
    if (threatAgentIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Threat Agent", 80, 420);

    ctx.fillStyle = "black";
    if (attackMethodIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Attack Method", 80, 470);

    ctx.fillStyle = "black";
    if (riskTreatmentIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Risk Treatment", 80, 520);

    ctx.fillStyle = "black";
    if (securityRequirementIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Security Requirement", 80, 570);

    ctx.fillStyle = "black";
    if (controlIsActive) {
        ctx.fillStyle = "green";
    }
    ctx.fillText("Control", 80, 620);
}

function handleLegendClick(e) {
    let canvasOffset = $("#legend").offset();
    let offsetY = canvasOffset.top;

    let mouseY = parseInt(e.pageY - offsetY);

    if (mouseY <= 30) {
        businessAssetIsActive = !businessAssetIsActive;
    } else if (mouseY >= 50 && mouseY <= 80) {
        systemAssetIsActive = !systemAssetIsActive;
    } else if (mouseY >= 100 && mouseY <= 130) {
        securityCriterionIsActive = !securityCriterionIsActive;
    } else if (mouseY >= 150 && mouseY <= 180) {
        eventIsActive = !riskIsActive;
        impactIsActive = !riskIsActive;
        threatIsActive = !riskIsActive;
        vulnerabilityIsActive = !riskIsActive;
        threatAgentIsActive = !riskIsActive;
        attackMethodIsActive = !riskIsActive;
        riskIsActive = !riskIsActive;
    } else if (mouseY >= 200 && mouseY <= 230) {

        eventIsActive = !eventIsActive;
        if (eventIsActive) {
            threatIsActive = true;
            vulnerabilityIsActive = true;
            threatAgentIsActive = true;
            attackMethodIsActive = true;
            if (impactIsActive) {
                riskIsActive = true;
            }
        } else {
            riskIsActive = false;
            threatIsActive = false;
            vulnerabilityIsActive = false;
            threatAgentIsActive = false;
            attackMethodIsActive = false;
        }

    } else if (mouseY >= 250 && mouseY <= 280) {
        impactIsActive = !impactIsActive;
        if (impactIsActive) {
            if (eventIsActive) {
                riskIsActive = true;
            }
        } else {
            riskIsActive = false;
        }
    } else if (mouseY >= 300 && mouseY <= 330) {
        threatIsActive = !threatIsActive;
        if (threatIsActive) {
            threatAgentIsActive = true;
            attackMethodIsActive = true;
            if (vulnerabilityIsActive && impactIsActive) {
                eventIsActive = true;
                riskIsActive = true;
            } else if (vulnerabilityIsActive) {
                eventIsActive = true;
            }
        } else {
            threatAgentIsActive = false;
            attackMethodIsActive = false;
            if (vulnerabilityIsActive && impactIsActive) {
                eventIsActive = false;
                riskIsActive = false;
            } else if (vulnerabilityIsActive) {
                eventIsActive = false;
            }
        }
    } else if (mouseY >= 350 && mouseY <= 380) {
        vulnerabilityIsActive = !vulnerabilityIsActive;
        if (vulnerabilityIsActive) {
            if (threatIsActive && impactIsActive) {
                eventIsActive = true;
                riskIsActive = true;
            } else if (threatIsActive) {
                eventIsActive = true;
            }
        } else {
            eventIsActive = false;
            riskIsActive = false;
        }
    } else if (mouseY >= 400 && mouseY <= 430) {
        threatAgentIsActive = !threatAgentIsActive;
        if (threatAgentIsActive) {
            if (attackMethodIsActive && vulnerabilityIsActive && impactIsActive) {
                riskIsActive = true;
                eventIsActive = true;
                threatIsActive = true;
            }
            else if (attackMethodIsActive && vulnerabilityIsActive) {
                threatIsActive = true;
                eventIsActive = true;
            }
            else if (attackMethodIsActive) {
                threatIsActive = true;
            }
        } else {
            threatIsActive = false;
            eventIsActive = false;
            riskIsActive = false;
        }
    } else if (mouseY >= 450 && mouseY <= 480) {
        attackMethodIsActive = !attackMethodIsActive;
        if (attackMethodIsActive) {
            if (threatAgentIsActive && vulnerabilityIsActive && impactIsActive) {
                riskIsActive = true;
                eventIsActive = true;
                threatIsActive = true;
            } else if (threatAgentIsActive && vulnerabilityIsActive) {
                threatIsActive = true;
                eventIsActive = true;
            } else if (threatAgentIsActive) {
                threatIsActive = true;
            }
        } else {
            threatIsActive = false;
            eventIsActive = false;
            riskIsActive = false;
        }
    } else if (mouseY >= 500 && mouseY <= 530) {
        riskTreatmentIsActive = !riskTreatmentIsActive;
    } else if (mouseY >= 550 && mouseY <= 580) {
        securityRequirementIsActive = !securityRequirementIsActive;
    } else if (mouseY >= 600 && mouseY <= 630) {
        controlIsActive = !controlIsActive;
    }

    syncDropDowns();
    drawAllAgain();
}

$(function () {
    drawLegend();
    $("#legend").click(handleLegendClick);
});