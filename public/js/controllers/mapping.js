
var systemAssetIsActive = false;
var businessAssetIsActive = false;
var securityCriterionIsActive = false;
var riskIsActive = false;
var impactIsActive = false;
var eventIsActive = false;
var attackMethodIsActive = false;
var vulnerabilityIsActive = false;
var threatAgentIsActive = false;
var threatIsActive = false;
var riskTreatmentIsActive = false;
var securityRequirementIsActive = false;
var controlIsActive = false;
var mapping = new Map();

mapping.set("Business Asset", ["Actor",
    "Activity-MAL1", "Activity-MAL2", "Decision-MAL", "ControlFlow-MAL", "UseCase", "SwimLane-MAL",
    "ActorTROPOS1", "ActorTROPOS2", "HardGoal1", "HardGoal2", "HardGoal3", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "ResourceTROPOS1", "ResourceTROPOS2", "ResourceTROPOS3", "SoftGoal1", "SoftGoal2", "SoftGoal3", "DependencyTROPOS", "MeansEndTROPOS", "ContributionTROPOS", "DecompositionTROPOS", "ResourceTROPOS4",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2", "DataObjectBPMN1", "DataObjectBPMN3"]);
mapping.set("System Asset", ["Actor", "IncludeUC", "ExtendUC",
    "Activity-MAL1", "Activity-MAL2", "Decision-MAL", "ControlFlow-MAL", "UseCase", "SwimLane-MAL",
    "ActorTROPOS1", "ActorTROPOS2", "HardGoal1", "HardGoal2", "HardGoal3", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "ResourceTROPOS1", "ResourceTROPOS2", "ResourceTROPOS3", "SoftGoal1", "SoftGoal2", "SoftGoal3", "DependencyTROPOS", "MeansEndTROPOS", "ContributionTROPOS", "DecompositionTROPOS", "ResourceTROPOS4",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2", "DataStoreBPMN1", "DataStoreBPMN3", "PoolBPMN1", "LaneBPMN1", "PoolBPMN2"]);
mapping.set("Security Criterion", ["SecurityCriterion", "Constraint_OfUC",
    "Secure_DependencyTROPOS", "Depender_SCTROPOS", "Dependee_SCTROPOS", "SecurityCriterion-MAL",
    "SoftGoal1", "SoftGoal2", "SoftGoal3", "SecurityConstraintTROPOS1", "SecurityConstraintTROPOS2", "SecurityConstraintTROPOS3", "ContributionTROPOS", "SC_DecompositionTROPOS", "RestrictsTROPOS",
    "AnnotationBPMN1", "AnnotationBPMN3", "LockBPMN1", "LockBPMN3"]);
mapping.set("Risk", ["Misuser", "Vulnerability", "MisUseCase", "Impact", "IncludeUC", "ExtendUC", "CommunicationUC", "ExploitUC", "ThreatenUC", "Lead_ToUC", "NegateUC", "HarmUC",
    "ControlFlow-MAL", "MalActivity-MAL1", "MalActivity-MAL2", "MalDecision-MAL", "MalSwimLane-MAL", "Vulnerability-MAL",
    "ThreatTROPOS3", "ActorTROPOS1", "ActorTROPOS2", "HardGoal1", "HardGoal2", "HardGoal3", "HardGoal4", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "PlanTROPOS4", "ExploitsTROPOS4", "AttacksTROPOS4", "ActorTROPOS4",
    "PoolBPMN1", "LaneBPMN1", "PoolBPMN2",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2", "DataFlowBPMN1", "DataFlowBPMN2",
    "AnnotationBPMN1", "AnnotationBPMN3"]);
mapping.set("Impact", ["Impact", "NegateUC", "HarmUC",
    "MalActivity-MAL1", "MalActivity-MAL2",
    "LockBPMN1", "LockBPMN3"]);
mapping.set("Event", ["Misuser", "MisUseCase", "Vulnerability", "IncludeUC", "ExtendUC", "CommunicationUC", "ExploitUC", "ThreatenUC", "Lead_ToUC",
    "ControlFlow-MAL", "MalActivity-MAL1", "MalActivity-MAL2", "MalDecision-MAL", "MalSwimLane-MAL",
    "Vulnerability-MAL",
    "ThreatTROPOS3", "ActorTROPOS1", "ActorTROPOS2", "HardGoal1", "HardGoal2", "HardGoal3", "HardGoal4", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "PlanTROPOS4", "ExploitsTROPOS4", "AttacksTROPOS4", "ActorTROPOS4",
    "PoolBPMN1", "LaneBPMN1", "PoolBPMN2",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2", "DataFlowBPMN1", "DataFlowBPMN2",
    "AnnotationBPMN1", "AnnotationBPMN3"]);
mapping.set("Attack Method", ["MisUseCase",
    "ControlFlow-MAL", "MalActivity-MAL1", "MalActivity-MAL2", "MalDecision-MAL", "MalSwimLane-MAL",
    "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "PlanTROPOS4",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2", "DataFlowBPMN1", "DataFlowBPMN2"]);
mapping.set("Vulnerability", ["Vulnerability", "IncludeUC", "ExtendUC",
    "Vulnerability-MAL",
    "AnnotationBPMN1", "AnnotationBPMN3", "VulnerabilityPointBPMN1", "VulnerabilityPointBPMN3"]);
mapping.set("Threat Agent", ["Misuser", "CommunicationUC",
    "MalSwimLane-MAL",
    "ActorTROPOS1", "ActorTROPOS2", "ActorTROPOS4",
    "PoolBPMN1", "LaneBPMN1", "PoolBPMN2"]);
mapping.set("Threat", ["Misuser", "MisUseCase", "CommunicationUC", "ExploitUC", "ThreatenUC",
    "ControlFlow-MAL", "MalActivity-MAL1", "MalActivity-MAL2", "MalDecision-MAL", "MalSwimLane-MAL",
    "HardGoal1", "HardGoal2", "HardGoal3", "HardGoal4", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "PlanTROPOS4", "ExploitsTROPOS4", "AttacksTROPOS4", "ActorTROPOS4",
    "PoolBPMN1", "LaneBPMN1", "PoolBPMN2",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2", "DataFlowBPMN1", "DataFlowBPMN2"]);

//NO RISK TREATMENT
mapping.set("Security Requirement", ["SecurityUseCase", "MitigateUC",
    "MitigationActivity-MAL1", "MitigationActivity-MAL2", "Decision-MAL", "ControlFlow-MAL", "MitigationLink-MAL",
    "SecureGoal1", "SecureGoal2", "ActorTROPOS1", "ActorTROPOS2", "HardGoal1", "HardGoal2", "HardGoal3", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "ResourceTROPOS1", "ResourceTROPOS2", "ResourceTROPOS3", "SoftGoal1", "SoftGoal2", "SoftGoal3", "DependencyTROPOS", "MeansEndTROPOS", "ContributionTROPOS", "DecompositionTROPOS", "SatisfiesTROPOS3", "MitigatesTROPOS3",
    "StartEventBPMN1", "IntermediateEventBPMN1", "EndEventBPMN1", "EventBPMN2", "GatewayBPMN1", "GatewayBPMN2", "TaskBPMN1", "TaskBPMN2", "TaskBPMN3", "SequenceFlowBPMN1", "SequenceFlowBPMN2"]);
mapping.set("Risk Treatment", []);
mapping.set("Control", ["SwimLane-MAL",
    "SecureGoal1", "SecureGoal2", "ActorTROPOS1", "ActorTROPOS2", "HardGoal1", "HardGoal2", "HardGoal3", "PlanTROPOS1", "PlanTROPOS2", "PlanTROPOS3", "ResourceTROPOS1", "ResourceTROPOS2", "ResourceTROPOS3", "SoftGoal1", "SoftGoal2", "SoftGoal3", "DependencyTROPOS", "MeansEndTROPOS", "ContributionTROPOS", "DecompositionTROPOS", "SatisfiesTROPOS3"]);

var createActiveArray = function () {
    var array = [];
    if (systemAssetIsActive) {
        array.push("System Asset");
    }
    if (businessAssetIsActive) {
        array.push("Business Asset");
    }
    if (securityCriterionIsActive) {
        array.push("Security Criterion");
    }
    if (riskIsActive) {
        array.push("Risk");
    }
    if (impactIsActive) {
        array.push("Impact");
    }
    if (eventIsActive) {
        array.push("Event");
    }
    if (attackMethodIsActive) {
        array.push("Attack Method");
    }
    if (vulnerabilityIsActive) {
        array.push("Vulnerability");
    }
    if (threatAgentIsActive) {
        array.push("Threat Agent");
    }
    if (threatIsActive) {
        array.push("Threat");
    }
    if (riskTreatmentIsActive) {
        array.push("Risk Treatment");
    }
    if (securityRequirementIsActive) {
        array.push("Security Requirement");
    }
    if (controlIsActive) {
        array.push("Control");
    }
    return array;
};
var rect = (function () {


    function rect(id, x, y, width, height, fill, stroke, strokewidth, activeArray, type) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.width = width;
        this.height = height;
        this.fill = fill || "white";
        this.stroke = stroke || "black";
        this.strokewidth = strokewidth || 2;
        this.activeArray = activeArray || [];
        this.type = type;
        this.redraw(this.x, this.y, activeArray);
        return (this);
    }

    rect.prototype.redraw = function (x, y, activeArray) {
        this.x = x || this.x;
        this.y = y || this.y;
        this.draw(this.stroke, activeArray);
        return (this);
    };

    function getFillStyle() {
        if (activeElements[i] === "Threat Agent") {
            return "#e6005c";
        }
        if (activeElements[i] === "Security Criterion") {
            return "#809fff";
        }
        if (activeElements[i] === "Risk") {
            return "#ff6666";
        }
        if (activeElements[i] === "Impact") {
            return "#e60000";
        }
        if (activeElements[i] === "Event") {
            return "#ff3333";
        }
        if (activeElements[i] === "Attack Method") {
            return "#ff3385";
        }
        if (activeElements[i] === "Vulnerability") {
            return "#ff9999";
        }
        if (activeElements[i] === "Threat") {
            return "#ffb3b3";
        }
        if (activeElements[i] === "Security Requirement") {
            return "#1f7a1f";
        }
        if (activeElements[i] === "System Asset") {
            return "#0039e6";
        }
        if (activeElements[i] === "Business Asset") {
            return "#94DCE5";
        }
        if (activeElements[i] === "Risk Treatment") {
            return "#85e085";
        }
        if (activeElements[i] === "Control") {
            return "#00ff55";
        }
    }

    rect.prototype.draw = function (stroke, activeArray) {
        activeElements = [];
        ctx.save();
        ctx.beginPath();
        ctx.fillStyle = this.fill;
        ctx.strokeStyle = stroke;
        ctx.lineWidth = this.strokewidth;
        for (i = 0; i < activeArray.length; i++) {
            if (mapping.get(activeArray[i]).includes(this.id)) {
                activeElements.push(activeArray[i]);
            }
        }

        if (activeElements.length > 0) {
            numberOfActiveElements = activeElements.length;
            sectorWidth = this.width / numberOfActiveElements;
            for (i = 0; i < activeElements.length; i++) {
                ctx.beginPath();
                ctx.fillStyle = this.fill;
                ctx.strokeStyle = stroke;
                ctx.lineWidth = this.strokewidth;
                ctx.fillStyle = getFillStyle();

                ctx.rect(this.x + i * sectorWidth, this.y, sectorWidth, this.height);
                ctx.fill();
                ctx.restore();
            }
        }
        else {
            ctx.beginPath();
            ctx.fillStyle = this.fill;
            ctx.strokeStyle = stroke;
            ctx.lineWidth = this.strokewidth;
            ctx.rect(this.x, this.y, this.width, this.height);
            ctx.stroke();
            ctx.fill();
            ctx.restore();
        }
    };

    rect.prototype.isPointInside = function (x, y) {
        return (x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height);
    };

    return rect;
})();
