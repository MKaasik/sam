
var rects = [];
function drawTroposText() {
    var canvas = document.getElementById("TROPOScanvas");
    var ctx = canvas.getContext("2d");


    ctx.font = "italic 30px Arial";
    ctx.fillStyle = "black";
    ctx.fillText("TROPOS diagrams", 150, 50);

    ctx.font = "10px Arial";

    ctx.fillText("Dependum", 80, 120);
    ctx.fillText("Dependency", 330, 120);
    ctx.fillText("Secure_Dependency", 500, 120);
    ctx.fillText("Goal", 40, 220);
    ctx.fillText("Plan", 140, 220);
    ctx.fillText("Resource", 240, 220);
    ctx.fillText("Actor", 340, 220);
    ctx.fillText("Depender_SC", 470, 220);
    ctx.fillText("Dependee_SC", 570, 220);
    ctx.fillText("Softgoal", 40, 320);
    ctx.fillText("Hardgoal", 140, 320);
    ctx.fillText("Security_Constraint", 320, 320);

    //relationship labels

    ctx.fillText("includes", 200, 110);
    ctx.fillText("dependee", 270, 160);
    ctx.fillText("depends", 400, 160);
    ctx.fillText("restricts", 290, 290);
    ctx.fillText("restricts", 230, 295);
    ctx.fillText("restricts", 230, 350);
    ctx.fillText("contains", 450, 310);

    //Second Diagram

    ctx.fillText("Contributor", 240, 420);
    ctx.fillText("Contribution", 530, 420);
    ctx.fillText("Decomposition", 90, 520);
    ctx.fillText("Role_From", 240, 520);
    ctx.fillText("Means-End", 380, 520);
    ctx.fillText("Security_Constraint", 20, 620);
    ctx.fillText("Plan", 140, 620);
    ctx.fillText("Resource", 240, 620);
    ctx.fillText("HardGoal", 340, 620);
    ctx.fillText("Secure_Goal", 430, 620);
    ctx.fillText("SoftGoal", 540, 620);
    ctx.fillText("SC_Decomposition", 40, 720);
    ctx.fillText("Actor", 240, 780);
    ctx.fillText("Goal", 440, 720);

    //relationship labels
    ctx.fillText("from", 360, 410);
    ctx.fillText("from", 180, 510);
    ctx.fillText("from", 320, 510);
    ctx.fillText("to", 590, 520);
    ctx.fillText("to", 120, 560);
    ctx.fillText("to", 385, 560);
    ctx.fillText("to", 440, 560);
    ctx.fillText("to", 60, 690);
    ctx.fillText("from", 90, 690);
    ctx.fillText("uses", 270, 690);
    ctx.fillText("executes", 190, 755);
    ctx.fillText("imposed to", 50, 795);
    ctx.fillText("has", 370, 770);

    //Third diagram
    ctx.fillText("Impact", 140, 870);
    ctx.fillText("Goal", 440, 870);
    ctx.fillText("Threat", 40, 970);
    ctx.fillText("Plan", 140, 970);
    ctx.fillText("Resource", 240, 970);
    ctx.fillText("HardGoal", 340, 970);
    ctx.fillText("SoftGoal", 440, 970);
    ctx.fillText("SecureGoal", 540, 970);
    ctx.fillText("Mitigates", 40, 1070);
    ctx.fillText("Restricts", 240, 1070);
    ctx.fillText("Satisfies", 540, 1070);
    ctx.fillText("SecurityConstraint", 220, 1170);

    //labels
    ctx.fillText("to", 270, 860);
    ctx.fillText("from", 70, 910);
    ctx.fillText("to", 170, 910);
    ctx.fillText("to", 270, 910);
    ctx.fillText("to", 70, 1040);
    ctx.fillText("restrictee", 170, 1040);
    ctx.fillText("restrictee", 270, 1040);
    ctx.fillText("restrictee", 370, 1040);
    ctx.fillText("satisfier", 570, 1040);
    ctx.fillText("from", 70, 1160);
    ctx.fillText("restrictor", 280, 1120);
    ctx.fillText("restrictor", 480, 1160);

    //fourth diagram
    ctx.font = "italic 10px Arial";
    ctx.fillText("Target", 100, 1320);
    ctx.font = "10px Arial";
    ctx.fillText("Exploits", 140, 1420);
    ctx.fillText("Attacks", 340, 1420);
    ctx.fillText("Hardgoal", 40, 1520);
    ctx.fillText("Plan", 240, 1520);
    ctx.fillText("Resource", 440, 1520);
    ctx.fillText("Actor", 240, 1620);

    //labels
    ctx.fillText("target", 140, 1380);
    ctx.fillText("exploited in", 180, 1460);
    ctx.fillText("executed in", 290, 1480);
    ctx.fillText("target", 420, 1480);
    ctx.fillText("has", 160, 1610);
    ctx.fillText("executes", 270, 1570);
    ctx.fillText("uses", 370, 1610);

}

function drawTroposLines() {
    var canvas = document.getElementById("TROPOScanvas");
    var ctx = canvas.getContext("2d");

    ctx.moveTo(140, 120);
    ctx.lineTo(320, 120);

    ctx.moveTo(400, 120);
    ctx.lineTo(500, 120);

    ctx.moveTo(60, 200);
    ctx.lineTo(60, 170);
    ctx.lineTo(260, 170);
    ctx.lineTo(260, 200);
    ctx.moveTo(160, 200);
    ctx.lineTo(160, 170);
    ctx.moveTo(100, 140);
    ctx.lineTo(100, 170);

    ctx.moveTo(340, 140);
    ctx.lineTo(340, 200);

    ctx.moveTo(380, 140);
    ctx.lineTo(380, 200);

    ctx.moveTo(520, 140);
    ctx.lineTo(520, 200);
    ctx.moveTo(570, 140);
    ctx.lineTo(570, 200);

    ctx.moveTo(580, 120);
    ctx.lineTo(660, 120);
    ctx.lineTo(660, 320);
    ctx.lineTo(400, 320);

    ctx.moveTo(60, 240);
    ctx.lineTo(60, 300);
    ctx.moveTo(60, 270);
    ctx.lineTo(160, 270);
    ctx.lineTo(160, 300);

    ctx.moveTo(200, 330);
    ctx.lineTo(320, 330);

    ctx.moveTo(280, 240);
    ctx.lineTo(280, 310);
    ctx.lineTo(320, 310);

    ctx.moveTo(180, 240);
    ctx.lineTo(240, 320);
    ctx.lineTo(320, 320);

    //Second diagram
    ctx.moveTo(300, 420);
    ctx.lineTo(520, 420);

    ctx.moveTo(40, 600);
    ctx.lineTo(40, 470);
    ctx.lineTo(540, 470);
    ctx.lineTo(540, 600);
    ctx.moveTo(260, 440);
    ctx.lineTo(260, 500);

    ctx.moveTo(160, 520);
    ctx.lineTo(220, 520);

    ctx.moveTo(300, 520);
    ctx.lineTo(360, 520);

    ctx.moveTo(140, 540);
    ctx.lineTo(140, 600);

    ctx.moveTo(160, 600);
    ctx.lineTo(160, 570);
    ctx.lineTo(360, 570);
    ctx.lineTo(360, 600);
    ctx.moveTo(260, 540);
    ctx.lineTo(260, 600);

    ctx.moveTo(380, 540);
    ctx.lineTo(380, 600);
    ctx.moveTo(430, 540);
    ctx.lineTo(430, 600);

    ctx.moveTo(580, 440);
    ctx.lineTo(580, 600);

    ctx.moveTo(30, 640);
    ctx.lineTo(30, 780);
    ctx.lineTo(220, 780);

    ctx.moveTo(50, 640);
    ctx.lineTo(50, 700);
    ctx.moveTo(80, 640);
    ctx.lineTo(80, 700);

    ctx.moveTo(160, 640);
    ctx.lineTo(160, 770);
    ctx.lineTo(220, 770);

    ctx.moveTo(260, 640);
    ctx.lineTo(260, 760);

    ctx.moveTo(300, 780);
    ctx.lineTo(460, 780);
    ctx.lineTo(460, 740);

    ctx.moveTo(360, 640);
    ctx.lineTo(360, 670);
    ctx.lineTo(560, 670);
    ctx.lineTo(560, 640);
    ctx.moveTo(460, 640);
    ctx.lineTo(460, 700);

    //Third diagram

    ctx.moveTo(200, 870);
    ctx.lineTo(420, 870);

    ctx.moveTo(60, 950);
    ctx.lineTo(60, 860);
    ctx.lineTo(120, 860);

    ctx.moveTo(160, 890);
    ctx.lineTo(160, 950);

    ctx.moveTo(200, 880);
    ctx.lineTo(260, 880);
    ctx.lineTo(260, 950);

    ctx.moveTo(360, 950);
    ctx.lineTo(360, 920);
    ctx.lineTo(560, 920);
    ctx.lineTo(560, 950);

    ctx.moveTo(460, 950);
    ctx.lineTo(460, 890);

    ctx.moveTo(60, 990);
    ctx.lineTo(60, 1050);


    ctx.moveTo(160, 990);
    ctx.lineTo(160, 1070);

    ctx.lineTo(220, 1070);
    ctx.moveTo(260, 990);
    ctx.lineTo(260, 1050);
    ctx.moveTo(300, 1070);
    ctx.lineTo(360, 1070);
    ctx.lineTo(360, 990);

    ctx.moveTo(560, 990);
    ctx.lineTo(560, 1050);

    ctx.moveTo(60, 1090);
    ctx.lineTo(60, 1170);
    ctx.lineTo(220, 1170);

    ctx.moveTo(260, 1090);
    ctx.lineTo(260, 1150);

    ctx.moveTo(300, 1170);
    ctx.lineTo(560, 1170);
    ctx.lineTo(560, 1090);

    //fourth diagram
    ctx.moveTo(120, 1300);
    ctx.lineTo(120, 1250);
    ctx.lineTo(480, 1250);
    ctx.lineTo(480, 1500);

    ctx.moveTo(120, 1250);
    ctx.lineTo(60, 1250);
    ctx.lineTo(60, 1500);

    ctx.moveTo(60, 1540);
    ctx.lineTo(60, 1620);
    ctx.lineTo(220, 1620);
    ctx.moveTo(300, 1620);
    ctx.lineTo(480, 1620);
    ctx.lineTo(480, 1540);

    ctx.moveTo(260, 1600);
    ctx.lineTo(260, 1540);
    ctx.moveTo(260, 1500);
    ctx.lineTo(260, 1250);

    ctx.moveTo(130, 1340);
    ctx.lineTo(130, 1400);

    ctx.moveTo(160, 1440);
    ctx.lineTo(160, 1520);
    ctx.lineTo(220, 1520);

    ctx.moveTo(280, 1500);
    ctx.lineTo(280, 1420);
    ctx.lineTo(320, 1420);

    ctx.moveTo(400, 1420);
    ctx.lineTo(460, 1420);
    ctx.lineTo(460, 1500);


    ctx.stroke();

};

function drawTroposArrows() {

    drawArrow("TROPOScanvas", 100, 140, "UP", "white");
    drawArrow("TROPOScanvas", 180, 110, "LEFT", "black");
    drawArrow("TROPOScanvas", 330, 170, "DOWN", "black");
    drawArrow("TROPOScanvas", 400, 170, "DOWN", "black");

    drawArrow("TROPOScanvas", 60, 240, "UP", "white");
    drawArrow("TROPOScanvas", 300, 270, "UP", "black");
    drawArrow("TROPOScanvas", 220, 290, "LEFT", "black");
    drawArrow("TROPOScanvas", 220, 350, "LEFT", "black");
    drawArrow("TROPOScanvas", 440, 310, "LEFT", "black");

    //Second diagram
    drawArrow("TROPOScanvas", 340, 410, "LEFT", "black");
    drawArrow("TROPOScanvas", 260, 440, "UP", "white");
    drawArrow("TROPOScanvas", 210, 510, "RIGHT", "black");
    drawArrow("TROPOScanvas", 310, 510, "LEFT", "black");
    drawArrow("TROPOScanvas", 120, 580, "DOWN", "black");
    drawArrow("TROPOScanvas", 385, 580, "DOWN", "black");
    drawArrow("TROPOScanvas", 440, 580, "DOWN", "black");
    drawArrow("TROPOScanvas", 590, 540, "DOWN", "black");
    drawArrow("TROPOScanvas", 260, 540, "UP", "white");
    drawArrow("TROPOScanvas", 60, 670, "UP", "black");
    drawArrow("TROPOScanvas", 90, 670, "UP", "black");
    drawArrow("TROPOScanvas", 270, 670, "UP", "black");
    drawArrow("TROPOScanvas", 120, 790, "RIGHT", "black");
    drawArrow("TROPOScanvas", 170, 760, "LEFT", "black");
    drawArrow("TROPOScanvas", 400, 770, "RIGHT", "black");
    drawArrow("TROPOScanvas", 460, 700, "DOWN", "white");

    //third diagram
    drawArrow("TROPOScanvas", 260, 860, "LEFT", "black");
    drawArrow("TROPOScanvas", 460, 890, "UP", "white");
    drawArrow("TROPOScanvas", 70, 920, "DOWN", "black");
    drawArrow("TROPOScanvas", 170, 920, "DOWN", "black");
    drawArrow("TROPOScanvas", 270, 920, "DOWN", "black");
    drawArrow("TROPOScanvas", 70, 1020, "UP", "black");
    drawArrow("TROPOScanvas", 170, 1020, "UP", "black");
    drawArrow("TROPOScanvas", 270, 1020, "UP", "black");
    drawArrow("TROPOScanvas", 370, 1020, "UP", "black");
    drawArrow("TROPOScanvas", 570, 1020, "UP", "black");
    drawArrow("TROPOScanvas", 270, 1120, "DOWN", "black");
    drawArrow("TROPOScanvas", 100, 1160, "RIGHT", "black");
    drawArrow("TROPOScanvas", 470, 1160, "LEFT", "black");

    //fourth diagram
    drawArrow("TROPOScanvas", 120, 1300, "DOWN", "white");
    drawArrow("TROPOScanvas", 140, 1360, "UP", "black");
    drawArrow("TROPOScanvas", 180, 1480, "DOWN", "black");
    drawArrow("TROPOScanvas", 290, 1460, "DOWN", "black");
    drawArrow("TROPOScanvas", 450, 1460, "DOWN", "black");
    drawArrow("TROPOScanvas", 150, 1610, "LEFT", "black");
    drawArrow("TROPOScanvas", 270, 1580, "UP", "black");
    drawArrow("TROPOScanvas", 400, 1610, "RIGHT", "black");
}

function handleTROPOSMouseDown(e) {

    var canvas = document.getElementById("TROPOScanvas");
    var ctx = canvas.getContext("2d");
    var canvasOffset = $("#TROPOScanvas").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    mouseX = parseInt(e.pageX - offsetX);
    mouseY = parseInt(e.pageY - offsetY);
    $(".dropdown").toggle(false);
    var clicked = "";
    for (var i = 0; i < rects.length; i++) {
        if (rects[i].type == "TROPOS" && rects[i].isPointInside(mouseX, mouseY)) {
            clicked = rects[i].id;
        }
    }

    if (clicked.length > 0) {
        var name = "#dropdown" + clicked;
        $(name).toggle(true);
    }
}

function drawTroposAgain() {
    if ($("#TROPOScanvas").is(":hidden")) {
        return;
    }

    canvas = document.getElementById("TROPOScanvas");
    ctx = canvas.getContext("2d");
    canvasOffset = $("#TROPOScanvas").offset();
    offsetX = canvasOffset.left;
    offsetY = canvasOffset.top;
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    var activeArray = createActiveArray();
    rects.push(new rect("Dependum", 60, 100, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("DependencyTROPOS", 320, 100, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Secure_DependencyTROPOS", 500, 100, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Goal", 20, 200, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("PlanTROPOS1", 120, 200, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ResourceTROPOS1", 220, 200, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ActorTROPOS1", 320, 200, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Depender_SCTROPOS", 460, 200, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Dependee_SCTROPOS", 560, 200, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SoftGoal1", 20, 300, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("HardGoal1", 120, 300, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SecurityConstraintTROPOS1", 320, 300, 80, 40, "white", "black", 1, activeArray, "TROPOS"));

    //SECOND Diagram

    rects.push(new rect("Contributor", 220, 400, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ContributionTROPOS", 520, 400, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("DecompositionTROPOS", 80, 500, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Role_From", 220, 500, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("MeansEndTROPOS", 360, 500, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SecurityConstraintTROPOS2", 20, 600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("PlanTROPOS2", 120, 600, 80, 40, "white", "60black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ResourceTROPOS2", 220, 600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("HardGoal2", 320, 600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("HardGoal2", 320, 600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SecureGoal1", 420, 600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SoftGoal2", 520, 600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SC_DecompositionTROPOS", 40, 700, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ActorTROPOS2", 220, 760, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Goal", 420, 700, 80, 40, "white", "black", 1, activeArray, "TROPOS"));


    //Third diagram
    rects.push(new rect("Impact", 120, 850, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("Goal", 420, 850, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ThreatTROPOS3", 20, 950, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("PlanTROPOS3", 120, 950, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ResourceTROPOS3", 220, 950, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("HardGoal3", 320, 950, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SoftGoal3", 420, 950, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SecureGoal2", 520, 950, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("MitigatesTROPOS3", 20, 1050, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("RestrictsTROPOS", 220, 1050, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SatisfiesTROPOS3", 520, 1050, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("SecurityConstraintTROPOS3", 220, 1150, 80, 40, "white", "black", 1, activeArray, "TROPOS"));

    //Fourth diagram
    rects.push(new rect("Target", 80, 1300, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ExploitsTROPOS4", 120, 1400, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("AttacksTROPOS4", 320, 1400, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("HardGoal4", 20, 1500, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("PlanTROPOS4", 220, 1500, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ResourceTROPOS4", 420, 1500, 80, 40, "white", "black", 1, activeArray, "TROPOS"));
    rects.push(new rect("ActorTROPOS4", 220, 1600, 80, 40, "white", "black", 1, activeArray, "TROPOS"));



    drawTroposLines();
    drawTroposText();
    drawTroposArrows();
};

function toggleTROPOS() {
    if ($('#troposbutton').hasClass("btn-success")) {
        $('#troposbutton').removeClass("btn-success");
        $('#troposbutton').addClass("btn-danger");
    } else {
        $('#troposbutton').removeClass("btn-danger");
        $('#troposbutton').addClass("btn-success");
    }


    $("#TROPOScanvas").toggle();
    $("#tropostext").toggle();

    drawTroposAgain();
    syncTable();
    drawLegend();
    $("#TROPOScanvas").click(handleTROPOSMouseDown);
}


$(function(){

    $('#selectboxActorTropos1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownActorTROPOS1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxActorTropos2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownActorTROPOS2").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxHardGoal1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownHardGoal1").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxHardGoal2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownHardGoal2").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxHardGoal3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownHardGoal3").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxHardGoal4').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownHardGoal4").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxResourceTROPOS4').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownResourceTROPOS4").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxPlan1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownPlanTROPOS1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxPlan2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownPlanTROPOS2").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxPlan3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownPlanTROPOS3").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxResourceTROPOS1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownResourceTROPOS1").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxResourceTROPOS2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownResourceTROPOS2").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxResourceTROPOS3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownResourceTROPOS3").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxSecureGoal1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecureGoal1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxSecureGoal2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecureGoal2").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxSoftGoal1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSoftGoal1").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxSoftGoal2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSoftGoal2").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxSoftGoal3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSoftGoal3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSecurityConstraintTROPOS1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecurityConstraintTROPOS1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSecurityConstraintTROPOS2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecurityConstraintTROPOS2").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSecurityConstraintTROPOS3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecurityConstraintTROPOS3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxContributionTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownContributionTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSC_DecompositionTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSC_DecompositionTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDependencyTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDependencyTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxMeansEndTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMeansEndTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDecompositionTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDecompositionTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxPlanTROPOS4').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownPlanTROPOS4").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxActorTROPOS4').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownActorTROPOS4").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxRestrictsTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownRestrictsTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxExploitsTROPOS4').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownExploitsTROPOS4").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxMitigatesTROPOS3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownMitigatesTROPOS3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSatisfiesTROPOS3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSatisfiesTROPOS3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSecure_DependencyTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSecure_DependencyTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDependee_SCTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDependee_SCTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDepender_SCTROPOS').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDepender_SCTROPOS").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxAttacksTROPOS4').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownAttacksTROPOS4").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxThreatTROPOS3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownThreatTROPOS3").toggle(false);
            drawAllAgain();
        }
    });
});