var rects = [];

function drawBPMNText() {
    var canvas = document.getElementById("BPMNcanvas");
    var ctx = canvas.getContext("2d");


    ctx.font = "italic 30px Arial";
    ctx.fillStyle = "black";
    ctx.fillText("BPMN diagrams", 100, 50);

    ctx.font = "10px Arial";

    ctx.fillText("GraphicalObject", 365, 60);
    ctx.fillText("FlowObject", 105, 160);
    ctx.fillText("Container", 305, 160);
    ctx.fillText("Artefact", 505, 160);
    ctx.fillText("Flows", 675, 160);
    ctx.fillText("Event", 35, 360);
    ctx.fillText("Gateway", 125, 360);
    ctx.fillText("Task", 240, 360);
    ctx.fillText("Pool", 260, 260);
    ctx.fillText("Lane", 360, 260);
    ctx.fillText("DataStore", 410, 360);
    ctx.fillText("Dataobject", 510, 360);
    ctx.fillText("Annotation", 610, 360);
    ctx.fillText("SequenceFlow", 610, 240);
    ctx.fillText("DataFlow", 610, 290);
    //LINESPLIT
    ctx.fillText("DataAssociation-", 705, 285);
    ctx.fillText("Flow", 705, 295);

    ctx.fillText("AssociationFLow", 705, 240);
    ctx.fillText("StartEvent", 30, 460);
    ctx.fillText("IntermediateEvent", 125, 460);
    ctx.fillText("EndEvent", 90, 520);
    ctx.fillText("VulnerabilityPoint", 305, 560);
    ctx.fillText("Lock", 320, 660);

    //Second diagram
    ctx.fillText("TaskEvent", 260, 770);
    ctx.fillText("DataFlow", 360, 770);
    ctx.fillText("Pool", 460, 770);
    ctx.fillText("Artefact", 30, 870);
    ctx.fillText("Task", 240, 870);
    ctx.fillText("Event", 340, 870);
    ctx.fillText("Gateway", 430, 870);
    //LINE SPLIT
    ctx.fillText("DataAssociation-", 125, 965);
    ctx.fillText("Flow", 125, 975);
    //
    ctx.fillText("FlowObject", 330, 970);
    ctx.fillText("SequenceFlow", 525, 970);

    //THIRD DIAGRAM
    ctx.fillText("Artefact", 160, 1070);
    ctx.fillText("Task", 470, 1070);
    ctx.fillText("DataObject", 30, 1170);
    ctx.fillText("Annotation", 130, 1170);
    ctx.fillText("DataStore", 230, 1170);
    ctx.fillText("VulnerabilityPoint", 425, 1170);
    ctx.fillText("AssociationFlow", 325, 1270);
    ctx.fillText("Lock", 340, 1370);

    //LABELS
    ctx.fillText("contains", 200, 150);
    ctx.fillText("contains", 300, 320);
    ctx.fillText("contains", 400, 320);
    ctx.fillText("belongs to", 300, 460);
    ctx.fillText("belongs to", 445, 460);
    ctx.fillText("belongs to", 245, 650);
    ctx.fillText("belongs to", 400, 650);

    //Second diagram
    ctx.fillText("to", 320, 710);
    ctx.fillText("to", 420, 710);
    ctx.fillText("from", 320, 810);
    ctx.fillText("from", 420, 810);

    ctx.fillText("to", 120, 860);
    ctx.fillText("from", 40, 940);

    ctx.fillText("to", 200, 860);
    ctx.fillText("from", 220, 990);

    ctx.fillText("to", 450, 955);
    ctx.fillText("from", 450, 975);

    //THIRD DIAGRAM
    ctx.fillText("belongs to", 330, 1160);
    ctx.fillText("belongs to", 400, 1130);
    ctx.fillText("to", 250, 1280);
    ctx.fillText("from", 430, 1280);
    ctx.fillText("from", 370, 1320);
    ctx.fillText("belongs to", 450, 1360);
    ctx.fillText("belongs to", 250, 1360);


}

function drawBPMNLines() {
    var canvas = document.getElementById("BPMNcanvas");
    var ctx = canvas.getContext("2d");

    ctx.moveTo(140, 140);
    ctx.lineTo(140, 110);
    ctx.lineTo(700, 110);
    ctx.lineTo(700, 140);
    ctx.moveTo(340, 140);
    ctx.lineTo(340, 110);
    ctx.moveTo(540, 140);
    ctx.lineTo(540, 110);
    ctx.moveTo(400, 110);
    ctx.lineTo(400, 80);

    ctx.moveTo(140, 180);
    ctx.lineTo(140, 340);
    ctx.moveTo(60, 340);
    ctx.lineTo(60, 310);
    ctx.lineTo(260, 310);
    ctx.lineTo(260, 340);

    ctx.moveTo(180, 160);
    ctx.lineTo(300, 160);

    ctx.moveTo(340, 180);
    ctx.lineTo(340, 210);
    ctx.lineTo(280, 210);
    ctx.lineTo(280, 240);
    ctx.moveTo(280, 210);
    ctx.lineTo(380, 210);
    ctx.lineTo(380, 240);

    ctx.moveTo(280, 280);
    ctx.lineTo(280, 300);
    ctx.lineTo(330, 300);
    ctx.lineTo(330, 260);
    ctx.lineTo(320, 260);

    ctx.moveTo(380, 280);
    ctx.lineTo(380, 300);
    ctx.lineTo(430, 300);
    ctx.lineTo(430, 260);
    ctx.lineTo(420, 260);

    ctx.moveTo(540, 180);
    ctx.lineTo(540, 340);
    ctx.moveTo(540, 320);
    ctx.lineTo(440, 320);
    ctx.lineTo(440, 340);
    ctx.moveTo(540, 320);
    ctx.lineTo(640, 320);
    ctx.lineTo(640, 340);

    ctx.moveTo(680, 290);
    ctx.lineTo(700, 290);
    ctx.moveTo(690, 290);
    ctx.lineTo(690, 240);
    ctx.lineTo(680, 240);
    ctx.lineTo(700, 240);
    ctx.moveTo(690, 240);
    ctx.lineTo(690, 180);

    ctx.moveTo(60, 380);
    ctx.lineTo(60, 440);
    ctx.moveTo(60, 420);
    ctx.lineTo(110, 420);
    ctx.lineTo(110, 500);
    ctx.moveTo(110, 420);
    ctx.lineTo(160, 420);
    ctx.lineTo(160, 440);

    ctx.moveTo(240, 380);
    ctx.lineTo(240, 660);
    ctx.lineTo(300, 660);

    ctx.moveTo(280, 380);
    ctx.lineTo(280, 560);
    ctx.lineTo(300, 560);


    ctx.moveTo(380, 560);
    ctx.lineTo(440, 560);
    ctx.lineTo(440, 380);

    ctx.moveTo(380, 660);
    ctx.lineTo(540, 660);
    ctx.lineTo(540, 380);

    //Second diagram
    ctx.moveTo(290, 750);
    ctx.lineTo(290, 720);
    ctx.lineTo(370, 720);
    ctx.lineTo(370, 750);

    ctx.moveTo(290, 790);
    ctx.lineTo(290, 820);
    ctx.lineTo(370, 820);
    ctx.lineTo(370, 790);

    ctx.moveTo(390, 750);
    ctx.lineTo(390, 720);
    ctx.lineTo(470, 720);
    ctx.lineTo(470, 750);

    ctx.moveTo(390, 790);
    ctx.lineTo(390, 820);
    ctx.lineTo(470, 820);
    ctx.lineTo(470, 790);

    ctx.moveTo(270, 790);
    ctx.lineTo(270, 850);
    ctx.moveTo(270, 830);
    ctx.lineTo(360, 830);
    ctx.lineTo(360, 850);

    ctx.moveTo(100, 870);
    ctx.lineTo(140, 870);
    ctx.lineTo(140, 950);

    ctx.moveTo(80, 890);
    ctx.lineTo(80, 970);
    ctx.lineTo(120, 970);

    ctx.moveTo(180, 950);
    ctx.lineTo(180, 870);
    ctx.lineTo(220, 870);

    ctx.moveTo(200, 970);
    ctx.lineTo(240, 970);
    ctx.lineTo(240, 890);

    ctx.moveTo(260, 890);
    ctx.lineTo(260, 920);
    ctx.lineTo(360, 920);
    ctx.lineTo(360, 950);

    ctx.moveTo(360, 920);
    ctx.lineTo(360, 890);
    ctx.moveTo(360, 920);
    ctx.lineTo(460, 920);
    ctx.lineTo(460, 890);

    ctx.moveTo(400, 960);
    ctx.lineTo(520, 960);

    ctx.moveTo(400, 980);
    ctx.lineTo(520, 980);

//THIRD DIAGRAM
    ctx.moveTo(190, 1090);
    ctx.lineTo(190, 1150);
    ctx.moveTo(60, 1150);
    ctx.lineTo(60, 1120);
    ctx.lineTo(260, 1120);
    ctx.lineTo(260, 1150);

    ctx.moveTo(300, 1170);
    ctx.lineTo(420, 1170);

    ctx.moveTo(460, 1150);
    ctx.lineTo(460, 1090);

    ctx.moveTo(510, 1090);
    ctx.lineTo(510, 1370);
    ctx.lineTo(400, 1370);

    ctx.moveTo(320, 1370);
    ctx.lineTo(60, 1370);
    ctx.lineTo(60, 1190);

    ctx.moveTo(160, 1190);
    ctx.lineTo(160, 1270);
    ctx.lineTo(320, 1270);

    ctx.moveTo(400, 1270);
    ctx.lineTo(460, 1270);
    ctx.lineTo(460, 1190);

    ctx.moveTo(360, 1290);
    ctx.lineTo(360, 1350);

    ctx.stroke();
};

function drawBPMNArrows() {

    drawArrow("BPMNcanvas", 400, 80, "UP", "white");
    drawArrow("BPMNcanvas", 140, 180, "UP", "white");
    drawArrow("BPMNcanvas", 340, 180, "UP", "white");
    drawArrow("BPMNcanvas", 540, 180, "UP", "white");
    drawArrow("BPMNcanvas", 690, 180, "UP", "white");
    drawArrow("BPMNcanvas", 60, 380, "UP", "white");
    drawArrow("BPMNcanvas", 190, 145, "LEFT", "black");
    drawArrow("BPMNcanvas", 290, 315, "RIGHT", "black");
    drawArrow("BPMNcanvas", 390, 315, "RIGHT", "black");

    //SECOND DIAGRAM
    drawArrow("BPMNcanvas", 270, 790, "UP", "white");
    drawArrow("BPMNcanvas", 360, 950, "DOWN", "white");

    //THIRD DIAGRAM
    drawArrow("BPMNcanvas", 190, 1090, "UP", "white");
    drawArrow("BPMNcanvas", 240, 1280, "LEFT", "black");
    drawArrow("BPMNcanvas", 420, 1280, "RIGHT", "black");
    drawArrow("BPMNcanvas", 370, 1330, "DOWN", "black");

}

function handleBPMNMouseDown(e) {

    var canvas = document.getElementById("BPMNcanvas");
    var ctx = canvas.getContext("2d");
    var canvasOffset = $("#BPMNcanvas").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    mouseX = parseInt(e.pageX - offsetX);
    mouseY = parseInt(e.pageY - offsetY);

    $(".dropdown").toggle(false);
    var clicked = "";
    for (var i = 0; i < rects.length; i++) {
        if (rects[i].type == "BPMN" && rects[i].isPointInside(mouseX, mouseY)) {
            clicked = rects[i].id;
        }
    }

    if (clicked.length > 0) {
        var name = "#dropdown" + clicked;
        $(name).toggle(true);
    }
}

function drawBPMNAgain() {
    if ($("#BPMNcanvas").is(":hidden")) {
        return;
    }

    canvas = document.getElementById("BPMNcanvas");
    ctx = canvas.getContext("2d");
    canvasOffset = $("#BPMNcanvas").offset();
    offsetX = canvasOffset.left;
    offsetY = canvasOffset.top;
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    var activeArray = createActiveArray();
    rects.push(new rect("GraphicalObject", 360, 40, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("FlowObject", 100, 140, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("Container", 300, 140, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("Artefact", 500, 140, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("Flows", 660, 140, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("EventBPMN1", 20, 340, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("GatewayBPMN1", 120, 340, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("TaskBPMN1", 220, 340, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("PoolBPMN1", 240, 240, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("LaneBPMN1", 340, 240, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataStoreBPMN1", 400, 340, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataObjectBPMN1", 500, 340, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("AnnotationBPMN1", 600, 340, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("SequenceFlowBPMN1", 600, 220, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataFlowBPMN1", 600, 270, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataAssociationFlow", 700, 270, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("AssociationFLow", 700, 220, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("StartEventBPMN1", 20, 440, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("IntermediateEventBPMN1", 120, 440, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("EndEventBPMN1", 80, 500, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("VulnerabilityPointBPMN1", 300, 540, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("LockBPMN1", 300, 640, 80, 40, "white", "black", 1, activeArray, "BPMN"));

//SECOND DIAGRAM
    rects.push(new rect("TaskEvent", 250, 750, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataFlowBPMN2", 350, 750, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("PoolBPMN2", 450, 750, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("Artefact", 20, 850, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("TaskBPMN2", 220, 850, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("EventBPMN2", 320, 850, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("GatewayBPMN2", 420, 850, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataAssociationFlow", 120, 950, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("FlowObject", 320, 950, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("SequenceFlowBPMN2", 520, 950, 80, 40, "white", "black", 1, activeArray, "BPMN"));


    //THIRD DIAGRAM
    rects.push(new rect("Artefact", 150, 1050, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("TaskBPMN3", 450, 1050, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataObjectBPMN3", 20, 1150, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("AnnotationBPMN3", 120, 1150, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("DataStoreBPMN3", 220, 1150, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("VulnerabilityPointBPMN3", 420, 1150, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("AssociationFlow", 320, 1250, 80, 40, "white", "black", 1, activeArray, "BPMN"));
    rects.push(new rect("LockBPMN3", 320, 1350, 80, 40, "white", "black", 1, activeArray, "BPMN"));

    drawBPMNLines();
    drawBPMNText();
    drawBPMNArrows();
};

function toggleBPMN() {
    if ($('#bpmnbutton').hasClass("btn-success")) {
        $('#bpmnbutton').removeClass("btn-success");
        $('#bpmnbutton').addClass("btn-danger");
    } else {
        $('#bpmnbutton').removeClass("btn-danger");
        $('#bpmnbutton').addClass("btn-success");
    }


    $("#BPMNcanvas").toggle();
    $("#bpmntext").toggle();

    drawBPMNAgain();
    syncTable();
    drawLegend();
    $("#BPMNcanvas").click(handleBPMNMouseDown);
}

$(function () {

    $('#selectboxStartEventBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownStartEventBPMN1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxIntermediateEventBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownIntermediateEventBPMN1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxEndEventBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownEndEventBPMN1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxEventBPMN2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownEventBPMN2").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxGatewayBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownGatewayBPMN1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxGatewayBPMN2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownGatewayBPMN2").toggle(false);
            drawAllAgain();
        }
    });


    $('#selectboxTaskBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownTaskBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxTaskBPMN2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownTaskBPMN2").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxTaskBPMN3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownTaskBPMN3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSequenceFlowBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSequenceFlowBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxSequenceFlowBPMN2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownSequenceFlowBPMN2").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxDataObjectBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDataObjectBPMN1").toggle(false);
            drawAllAgain();
        }
    });

    $('#selectboxDataObjectBPMN3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDataObjectBPMN3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDataStoreBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDataStoreBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDataStoreBPMN3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDataStoreBPMN3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxPoolBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownPoolBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxLaneBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownLaneBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxPoolBPMN2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownPoolBPMN2").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxAnnotationBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownAnnotationBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxAnnotationBPMN3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownAnnotationBPMN3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDataFlowBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDataFlowBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxDataFlowBPMN2').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownDataFlowBPMN2").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxLockBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownLockBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxLockBPMN3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownLockBPMN3").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxVulnerabilityPointBPMN1').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownVulnerabilityPointBPMN1").toggle(false);
            drawAllAgain();
        }
    });
    $('#selectboxVulnerabilityPointBPMN3').multiselect({
        onChange: function (option, checked) {
            var opselected = $(option).val();
            setActiveState(opselected, checked);
            syncDropDowns();
            $("#dropdownVulnerabilityPointBPMN3").toggle(false);
            drawAllAgain();
        }
    });
});